|| [English](CHANGELOG.md) || [Nederlands](CHANGELOG_nl.md) || [français](CHANGELOG_fr.md) ||

# Change Log

## 0.1.1 (2018-02-23)

- Mobiel menu
- Wachtwoord voor emailloze klanten wordt eenmalig in de webapp getoond
- Firma's kunnen eigen url krijgen met eigen styling

## 0.1.0 (2018-02-21)

- Bugfix: Chrome popup verschijnt niet met slechte inloggegevens
- Logo en achtergrond kunnen eenvoudig globaal of per bedrijf worden gewijzigd
- Opmerkingen over clients kunnen meerdere regels bevatten
- Nederlands: naam -> voornaam
- Als op Enter wordt gedrukt, wordt het formulier verzonden
- Handmatig toegewezen punten zijn te zien in de klantgeschiedenis
- Ticketgegevens en toegewezen punten zijn te zien in de klantgeschiedenis
- Punten kunnen handmatig worden toegewezen per klant en per promotie

## 0.0.1 Eerste werkende versie
