import {Component, OnDestroy, OnInit} from '@angular/core';
import {PromoqHttpService} from '../../services/promoq-http.service';
import {Subscription} from 'rxjs/Subscription';
import {User} from '../../models/user';
import {GlobalService} from '../../services/global.service';
import {MatDialog} from '@angular/material';
import {NewFirmDialog} from '../../dialogs/new-firm-dialog/new-firm-dialog.component';
import {isUndefined} from 'util';
import {Firm} from '../../models/firm';
import {Page} from '../../hooks';
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-branches-firms',
  templateUrl: './branches-firms.component.html',
})

export class BranchesFirmsPageComponent implements Page, OnInit, OnDestroy {
  loggedInUserSubscription: Subscription;
  loggedInUser: User;
  public loading: boolean;
  public title = 'Branches';
  private maxFirms: number;

  constructor(private httpService: PromoqHttpService,
              public global: GlobalService,
              private dialog: MatDialog,
              private route: ActivatedRoute) {
  }

  public firms: Firm[];

  getFirms() {
    if (this.global.isLoggedIn()) {
      this.httpService.getAllFirms().then(
        (firms: Firm[]) => {
          this.firms = firms;
          if (this.global.isUserInRole('ROLE_OWNER')) {
            this.httpService.getMaxFirms().then(
              (maxFirms: number) => {
                this.maxFirms = maxFirms;
                this.loading = false;
              }
            );
          } else {
            this.loading = false;
          }
        }
      );
    }
  }

  ngOnInit() {
    this.global.setBranding(this.route);
    this.loggedInUserSubscription = this.global.getUser()
      .subscribe(user => {
        this.loading = true;
        this.loggedInUser = user;
        this.getFirms();
      });
    this.global.checkRole(this.title);
  }

  ngOnDestroy() {
    this.loggedInUserSubscription.unsubscribe();
  }

  private addFirm(firm: Firm) {
    this.httpService.addFirm(firm).then(
      (response: Firm) => this.firms.unshift(response),
      error => this.onAddFirmFailed(firm)
    );
  }

  private onAddFirmFailed(firm: Firm) {
    this.global.warningDialogTryAgain('adding', 'firm', firm.name)
      .then(ok => this.addFirm(firm));
  }

  onAddFirmClicked() {
    if (this.global.isLoggedIn()) {
      this.dialog.open(NewFirmDialog, {
        data: {userRole: this.global.getFirstRole()}
      }).afterClosed().subscribe(
        (response: Firm) => {
          if (!isUndefined(response)) {

            this.addFirm(response);
          }
        }
      );
    }
  }

  private deleteFirm(firm: Firm) {
    this.httpService.deleteFirm(firm.id).then(
      response => {
      },
      error => this.onDeleteFirmFailed(firm, error));
  }

  private onDeleteFirmFailed(firm: Firm, error) {
    this.firms.unshift(firm);
    this.global.warningDialogTryAgain('removing', 'firm', firm.name)
      .then(ok => this.deleteFirm(firm));
  }

  onDeleteFirmClicked(firm: Firm) {
    if (this.global.isLoggedIn()) {
      this.global.warningDialogRemoval('firm', firm.name)
      .then(
        ok => {
          firm = this.firms.find(x => x.id === firm.id);
          this.firms = this.firms.filter(x => x !== firm);
          this.deleteFirm(firm);
          }
      );
    }
    return null;
  }


  checkCanAddFirm() {
    return this.maxFirms > this.firms.length;
  }
}
