import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Branch} from '../../../models/branch';
import {User} from '../../../models/user';
import {PromoqHttpService} from '../../../services/promoq-http.service';
import {MatDialog} from '@angular/material';
import {BranchDetailsDialog} from '../../../dialogs/branch-details-dialog/branch-details-dialog.component';
import {NewBranchDialog} from '../../../dialogs/new-branch-dialog/new-branch-dialog.component';
import {isUndefined} from 'util';
import {Firm} from '../../../models/firm';
import {GlobalService} from '../../../services/global.service';
import {Loading} from '../../../hooks';

@Component({
  selector: 'app-branches',
  templateUrl: './branches.component.html',
})
export class BranchesComponent implements Loading, OnInit {
  public loading: boolean;
  @Input() firm: Firm;
  @Output() deleteFirm: EventEmitter<number> = new EventEmitter();

  constructor(private httpService: PromoqHttpService,
              public global: GlobalService,
              private dialog: MatDialog) {
  }

  private _branches: Branch[];

  get branches(): Branch[] {
    return this._branches;
  }

  set branches(value: Branch[]) {
    this._branches = value;
  }

  ngOnInit() {
    this.loading = true;
    this.getBranches();
  }

  public onBranchDetailsClicked(branch: Branch) {
    this.dialog.open(BranchDetailsDialog,
      {data: {branch: branch}, width: '90%'})
      .afterClosed().subscribe( (response: Branch) => {
        if (!isUndefined(response)) {
            this.updateBranch(response);
        }
    });
  }

  public onDeleteBranchClicked(id: Number, name: string) {
    if (this.global.isLoggedIn()) {
      this.global.warningDialogRemoval('branch', name)
        .then( ok => {
            const branch = this.branches.find(x => x.id === id);
            this.branches = this.branches.filter(x => x !== branch);
            this.deleteBranch(branch);
          }
      );
    }
  }

  public onDeleteFirmClicked() {
    this.deleteFirm.emit(this.firm.id);
  }

  public onAddBranchClicked() {
    this.dialog.open(NewBranchDialog).afterClosed().subscribe(
      (branch: Branch) => {
        if (!isUndefined(branch.name)) {
          branch.firmId = this.firm.id;
          this.addBranch(branch);
        }
      }
    );
  }

  private onAddBranchFailed(branch: Branch) {
    this.global.warningDialogTryAgain('adding', 'branch', branch.name)
      .then( ok => this.addBranch(branch));
  }

  private onUpdateBranchFailed(branch: Branch) {
    this.global.warningDialogTryAgain('updating', 'branch', branch.name)
      .then(ok => this.updateBranch(branch));
  }

  private onRemoveBranchFailed(branch: Branch, error, force) {
    this.branches.push(branch);
    switch (error.status) {
      case 409:
        this.global.warningDialogNotEmpty('branch', branch.name)
          .then(ok => this.deleteBranch(branch, true));
        break;
      default:
        this.global.warningDialogTryAgain('removing', 'branch', branch.name)
          .then(ok => this.deleteBranch(branch, force));
        break;
    }
  }

  private addBranch(branch: Branch) {
    this.httpService.addOrUpdateBranch(branch).then(
      (response: Branch) => {
        this.branches.push(response);
      },
      error => this.onAddBranchFailed(branch)
    );
  }

  private updateBranch(branch: Branch) {

    this.httpService.addOrUpdateBranch(branch).then(
      (response: Branch) => {
        this.getBranches();
      },
      error => this.onUpdateBranchFailed(branch)
    );
  }

  private deleteBranch(branch: Branch, force: boolean = false) {
    this.httpService.deleteBranch(branch.id, force).then(
      response => {
      },
      error => this.onRemoveBranchFailed(branch, error, force)
    );
  }

  private getBranches() {
    this.httpService.getAllBranchesForFirm(this.firm.id).then(
      (response: Branch[]) => {
        this.branches = response;
        this.loading = false;
      }, error => {
        this.loading = false;
      }
    );
  }
}
