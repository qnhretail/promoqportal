import {Component, Input} from '@angular/core';
import {User} from '../../../models/user';
import {Firm} from '../../../models/firm';
import {MatDialog} from '@angular/material';
import {NewPromotionDialog} from '../../../dialogs/new-promotion-dialog/new-promotion-dialog.component';
import {PromoqHttpService} from '../../../services/promoq-http.service';
import {GlobalService} from '../../../services/global.service';
import {Promotion} from '../../../models/promotion';
import {isUndefined} from 'util';
import {Loading} from '../../../hooks';

@Component({
  selector: 'app-promotions',
  templateUrl: './promotions.component.html',
})
export class PromotionsComponent implements Loading {
  public loading: boolean;
  promotions: Promotion[] = [];
  @Input() firm: Firm;
  @Input() loggedInUser: User;

  constructor(private httpService: PromoqHttpService,
              private global: GlobalService,
              private dialog: MatDialog) {
  }

  ngOnInit() {
    this.loading = true;
    this.getPromotions();
  }

  private getPromotions() {
    this.httpService.getAllPromotionsFromFirm(this.firm.id).then(
      (result: Promotion[]) => {
        this.promotions = result;
        this.loading = false;
      }, error => this.loading = false
    );
  }

  public onAddPromotionClicked() {
    this.dialog.open(NewPromotionDialog,
      {
        data: {
          firm: this.firm,
          loggedInUser: this.loggedInUser
        }
      }).afterClosed().subscribe(
      (response: Promotion) => {
        if (!isUndefined(response)) {
          this.addPromotion(response);
        }
      }
    );
  }

  private addPromotion(promotion: Promotion) {
    this.httpService.addPromotion(promotion).then(
      (response: Promotion) => this.promotions.unshift(response),
      error => this.onAddPromotionFailed(promotion)
    );
  }

  private onAddPromotionFailed(promotion: Promotion) {
    this.global.warningDialogTryAgain('adding', 'promotion', promotion.name)
      .then(ok => this.addPromotion(promotion));
  }

  onDeletePromotionClicked(promotion: Promotion) {
    if (this.global.isLoggedIn()) {
      this.global.warningDialogRemoval('promotion', promotion.name)
        .then(
          ok => {
            this.promotions = this.promotions.filter(x => x !== promotion);
            this.deletePromotion(promotion);
          }
        );
    }
  }

  onPromotionDetailsClicked(promotion: Promotion) {
    this.dialog.open(NewPromotionDialog,
      {
        data: {
          promotion: promotion,
          firm: this.firm,
          loggedInUser: this.loggedInUser
        }
      }).afterClosed().subscribe(
      (response: Promotion) => {
        if (!isUndefined(response)) {
          true;
          //Todo
        }
      }
    );
  }

  private deletePromotion(promotion: Promotion) {
    this.httpService.deletePromotion(promotion.id).then(
      response => {
      },
      error => this.onRemovePromotionFailed(promotion, error)
    );
  }

  private onRemovePromotionFailed(promotion: Promotion, error: any) {
    this.promotions.unshift(promotion);
    this.global.warningDialogTryAgain('removing', 'promotion', promotion.name)
      .then(ok => this.deletePromotion(promotion));
  }
}
