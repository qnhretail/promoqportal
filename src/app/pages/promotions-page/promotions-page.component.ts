import {Component} from '@angular/core';
import {Firm} from '../../models/firm';
import {GlobalService} from '../../services/global.service';
import {Subscription} from 'rxjs/Subscription';
import {User} from '../../models/user';
import {PromoqHttpService} from '../../services/promoq-http.service';
import {Page} from '../../hooks';
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-promotions-page',
  templateUrl: './promotions-page.component.html',
})
export class PromotionsPageComponent implements Page {
  public loading: boolean;
  public firms: Firm[] = [];
  public loggedInUserSubscription: Subscription;
  public title = 'Promotions';
  public loggedInUser: User;

  constructor(private httpService: PromoqHttpService,
              private global: GlobalService,
              private route: ActivatedRoute) {
  }

  ngOnDestroy(): void {
  }

  ngOnInit() {
    this.global.setBranding(this.route);
    this.loading = true;
    this.loggedInUserSubscription = this.global.getUser()
      .subscribe(user => {
        this.loggedInUser = user;
        this.getFirms();
      });
    this.global.checkRole(this.title);
  }

  private getFirms() {
    this.httpService.getAllFirms().then(
      (response: Firm[]) => {
        this.firms = response;
        this.loading = false;
      }, error => this.loading = false
    );
  }

  public isUserOwnerOrStaff() {
    return this.global.isUserInRole('ROLE_OWNER') ||
      this.global.isUserInRole('ROLE_ADMIN') ||
      this.global.isUserInRole('ROLE_STAFF');
  }

}
