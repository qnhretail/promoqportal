import {Component, OnInit, ViewChild} from '@angular/core';
import {MailServerConfigurationPageComponent} from './mail-server-configuration-page/mail-server-configuration-page.component';
import {Page} from '../../hooks';
import {Subscription} from 'rxjs/Subscription';
import {User} from '../../models/user';
import {GlobalService} from "../../services/global.service";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-configuration-page',
  templateUrl: './configuration-page.component.html'
})
export class ConfigurationPageComponent implements Page {
  loggedInUserSubscription: Subscription;
  loggedInUser: User;
  loading: boolean;
  public title = 'Configuration';
  @ViewChild(MailServerConfigurationPageComponent)
  private mailServerConfiguration: MailServerConfigurationPageComponent;

  constructor(private global: GlobalService,
              private route: ActivatedRoute) {}

  ngOnInit() {
    this.global.setBranding(this.route);
    this.loggedInUserSubscription = this.global.getUser()
      .subscribe(user => {
        this.loading = true;
        this.loggedInUser = user;
      });
    this.global.checkRole(this.title);
  }

  ngOnDestroy(): void {
  }


  onSaveChangesClicked(tabIndex: number) {
    switch (tabIndex) {
      case 0: // Common
        break;
      case 1: // Email
        this.mailServerConfiguration.onSaveChangesClicked();
        break;
    }
  }
}
