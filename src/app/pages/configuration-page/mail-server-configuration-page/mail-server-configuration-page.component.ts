import {Component, Input, OnInit} from '@angular/core';
import {SmtpServerConfiguration} from '../../../models/smtpServerConfiguration';
import {PromoqHttpService} from '../../../services/promoq-http.service';
import {GlobalService} from '../../../services/global.service';
import {User} from '../../../models/user';
import {Loading} from '../../../hooks';

@Component({
  selector: 'app-mail-server-configuration-page',
  templateUrl: './mail-server-configuration-page.component.html',
})
export class MailServerConfigurationPageComponent implements Loading, OnInit {
  loading: boolean;
  public smtpServerConfiguration: SmtpServerConfiguration;
  originalSmtpServerConfiguration: SmtpServerConfiguration;
  public hide = true;
  public password = '';
  @Input() loggedInUser: User;

  constructor(private httpService: PromoqHttpService,
              private global: GlobalService) {
  }

  ngOnInit() {
    this.loading = true;
    this.getSmtpServerConfiguration();
  }

  public onSaveChangesClicked(): any {
    this.saveChanges();
  }

  public onUpdatePasswordClicked() {
    this.updatePassword();
  }

  private getSmtpServerConfiguration(): any {
    this.httpService.getSmtpServerConfiguration().then(
      (response: SmtpServerConfiguration) => {
        this.smtpServerConfiguration = response;
        this.originalSmtpServerConfiguration = response;
        this.loading = false;
      }
    );
  }

  private saveChanges() {
    this.httpService.saveSmtpServerConfiguration(this.smtpServerConfiguration).then(
      response => {},
      error => this.onSaveChangesFailed()
    );
  }

  private onSaveChangesFailed() {
    this.global.warningDialogTryAgain('saving', 'configuration', 'SMTP')
      .then(ok => this.saveChanges());
  }

  onUpdatePasswordFailed(): any {
    this.global.warningDialogTryAgain('updating', 'password', '')
      .then(ok => this.updatePassword());
  }

  private updatePassword() {
    this.httpService.updateSmtpServerPassword(this.password).then(
      response => this.password = '',
      error => this.onUpdatePasswordFailed()
    );
  }
}
