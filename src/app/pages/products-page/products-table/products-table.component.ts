import {Component, Input, OnInit} from '@angular/core';
import {Firm} from '../../../models/firm';
import {PromoqHttpService} from '../../../services/promoq-http.service';
import {GlobalService} from '../../../services/global.service';
import {MatDialog} from '@angular/material';
import {Product} from '../../../models/product';
import {isUndefined} from 'util';
import {NewProductDialog} from '../../../dialogs/new-product-dialog/new-product-dialog.component';

@Component({
  selector: 'app-products-table',
  templateUrl: './products-table.component.html',
})
export class ProductsTableComponent implements OnInit {
  @Input() firm: Firm;
  public products: Product[];

  constructor(private httpService: PromoqHttpService,
              public global: GlobalService,
              private dialog: MatDialog) { }

  ngOnInit() {
    this.getProducts();
  }

  private getProducts() {
    this.httpService.getAllProductsForFirm(this.firm.id).then(
      (products: Product[]) => {
        this.products = products;
      }
    );
  }

  updateProduct(product: Product) {
    this.httpService.updateProduct(product).then(ok => {
      this.getProducts();
    });
  }

  onProductDetailsClicked(product: Product) {
    this.dialog.open(NewProductDialog,
      {data: {product: product}})
      .afterClosed().subscribe((response: Product) => {
      if (!isUndefined(response)) {
        this.updateProduct(response);
      }
    });
  }

  public onAddProductClicked() {
    this.dialog.open(NewProductDialog, {data: {product: null}}).afterClosed().subscribe(
      (product: Product) => {
        product.firmId = this.firm.id;
        this.addProduct(product);
      }
    );
  }

  addProduct(product: Product) {
    this.httpService.addProduct(product).then(
      (result: Product) => this.getProducts(),
      (error: Error) => this.onAddProductFailed(product)
    );
  }

  onAddProductFailed(product: Product) {
  }
}
