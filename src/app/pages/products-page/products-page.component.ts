import {Component, OnInit} from '@angular/core';
import {Subscription} from 'rxjs/Subscription';
import {PromoqHttpService} from '../../services/promoq-http.service';
import {GlobalService} from '../../services/global.service';
import {Firm} from '../../models/firm';
import {User} from '../../models/user';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-products-page',
  templateUrl: './products-page.component.html',
})
export class ProductsPageComponent implements OnInit {
  firms: Firm[];
  loggedInUser: User;
  loading: boolean;
  public title= 'Products';
  private loggedInUserSubscription: Subscription;

  constructor(private httpService: PromoqHttpService,
              public global: GlobalService,
              private route: ActivatedRoute) { }

  ngOnInit() {
    this.global.setBranding(this.route);
    this.loggedInUserSubscription = this.global.getUser()
      .subscribe(user => {
        this.loading = true;
        this.loggedInUser = user;
        this.getFirms();
        this.global.checkRole(this.title);
      });
  }

  private getFirms() {
    this.httpService.getAllFirms().then(
      (firms: Firm[]) => {
        this.firms = firms;
      }
    );
  }

}
