import {Component} from '@angular/core';
import {Firm} from '../../models/firm';
import {Subscription} from 'rxjs/Subscription';
import {User} from '../../models/user';
import {PromoqHttpService} from '../../services/promoq-http.service';
import {GlobalService} from '../../services/global.service';
import {Page} from '../../hooks';
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-staff-page',
  templateUrl: './staff-page.component.html',
})
export class StaffPageComponent implements Page {
  public loading: boolean;
  public title = 'Staff';
  public loggedInUserSubscription: Subscription;
  public firms: Firm[] = [];
  public loggedInUser: User;

  ngOnDestroy(): void {
  }


  constructor(private httpService: PromoqHttpService,
              private global: GlobalService,
              private route: ActivatedRoute) {
  }


  ngOnInit() {
    this.global.setBranding(this.route);
    this.loading = true;
    this.loggedInUserSubscription = this.global.getUser()
      .subscribe(user => {
        this.loggedInUser = user;
        this.getFirms();
      });
    this.global.checkRole(this.title);
  }

  private getFirms() {
      this.httpService.getAllFirms().then(
        (response: Firm[]) => {
          this.firms = response;
          this.loading = false;
        }
      );
    }
}
