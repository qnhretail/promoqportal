import {Component, Input} from '@angular/core';
import {User} from '../../../models/user';
import {Firm} from '../../../models/firm';
import {PromoqHttpService} from '../../../services/promoq-http.service';
import {MatDialog} from '@angular/material';
import {StaffMember} from '../../../models/staff-member';
import {GlobalService} from '../../../services/global.service';
import {NewStaffMemberDialog} from '../../../dialogs/new-staff-member-dialog/new-staff-member-dialog.component';
import {isUndefined} from 'util';
import {Loading} from '../../../hooks';

@Component({
  selector: 'app-staff-list',
  templateUrl: './staff-list.component.html',
})
export class StaffListComponent implements Loading {
  public loading: boolean;
  @Input() loggedInUser: User;
  @Input() firm: Firm;
  public staff: StaffMember[] = [];

  constructor(private httpService: PromoqHttpService,
              private global: GlobalService,
              private dialog: MatDialog) {
  }

  ngOnInit() {
    this.loading = true;
    this.getStaff();
  }

  private getStaff() {
    this.httpService.getAllStaffMemberFromFirm(this.firm.id).then(
      (result: StaffMember[]) => {
        this.staff = result;
        this.loading = false;
      }, error => this.loading = false
    );
  }

  public static onResetPasswordClicked(id: number) {
  }

  public onAddStaffMemberClicked() {
    this.dialog.open(NewStaffMemberDialog).afterClosed().subscribe(
      (response: StaffMember) => {
        if (!isUndefined(response)) {
          response.firmId = this.firm.id;
          this.addStaffMember(response);
        }
      }
    );
  }

  private addStaffMember(staffMember: StaffMember) {
    this.httpService.addStaffMember(staffMember).then(
      (response: StaffMember) => this.staff.unshift(response),
      error => this.onAddStaffMemberFailed(staffMember)
    );
  }

  private onAddStaffMemberFailed(staffMember: StaffMember) {
    this.global.warningDialogTryAgain('adding', 'staff member', staffMember.username)
      .then(ok => this.addStaffMember(staffMember));
  }

  public onDeleteStaffMemberClicked(id: number) {
    this.global.warningDialogRemoval('staff member', id)
      .then(ok => null);
  }
}
