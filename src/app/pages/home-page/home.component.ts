import {Component, OnInit} from '@angular/core';
import {Page} from '../../hooks';
import {Subscription} from 'rxjs/Subscription';
import {User} from '../../models/user';
import {PromoqHttpService} from '../../services/promoq-http.service';
import {GlobalService} from '../../services/global.service';
import {Firm} from '../../models/firm';
import {environment} from '../../../environments/environment';
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
})
export class HomePageComponent implements Page, OnInit {
  badLogin = false;
  public password: string;
  public loggedInUserSubscription: Subscription;
  public loggedInUser: User;
  public firms: Firm[];
  public loading: boolean;
  public title = 'Home';
  public username: string;
  public hide = true;
  private companyName: string;

  constructor(private httpService: PromoqHttpService,
              private global: GlobalService,
              private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.global.setBranding(this.route);
    this.loading = true;
    this.loggedInUserSubscription = this.global.getUser()
      .subscribe(user => {
        this.loggedInUser = user;
        if (this.global.isUserInRole('ROLE_CLIENT')) this.getFirms();
        else {
          this.firms = [];
          this.loading = false;
        }
      });
  }

  private getFirms() {
    this.httpService.getAllFirmsWithPromotions().then(
      (response: Firm[]) => {
        this.firms = response;
        this.loading = false;
      }
    );
  }

  public loggedIn() {
    return this.global.isLoggedIn();
  }

  onLoginClicked(): void {
    if (this.username !== '' && this.password !== '') {
      const authToken: string = environment.authType + btoa(this.username + ':' + this.password.trim());
      this.httpService.login(authToken).then(
        response => {
          this.global.setUser(new User(response['username'], response['roles']));
          this.global.setBranding(this.route);
        },
        reject => {
          this.badLogin = true;
        });
    }
  }
}
