import {Component, Input} from '@angular/core';
import {Loading} from '../../../../../hooks';
import {Branch} from '../../../../../models/branch';
import {User} from '../../../../../models/user';
import {PromoqHttpService} from '../../../../../services/promoq-http.service';

@Component({
  selector: '[app-branch-short-description]',
  templateUrl: './branch-short-description.component.html',
})
export class BranchShortDescriptionComponent implements Loading {
  public loading: boolean;
  @Input() branchId: number;
  @Input() loggedInUser: User;
  public branch: Branch;

  constructor(private httpService: PromoqHttpService) {
  }

  ngOnInit() {
    this.loading = true;
    this.getBranch();
  }

  getBranch() {
    this.httpService.getBranchById(this.branchId).then(
      (result: Branch) => {
        this.branch = result;
        this.loading = false;
      }
    );
  }

}
