import {Component, Input} from '@angular/core';
import {Promotion} from '../../../../models/promotion';
import {Loading} from '../../../../hooks';
import {User} from '../../../../models/user';
import {PromoqHttpService} from '../../../../services/promoq-http.service';
import {GlobalService} from '../../../../services/global.service';

@Component({
  selector: 'app-promotion',
  templateUrl: './promotion.component.html',
})
export class PromotionComponent implements Loading {
  public loading: boolean;
  @Input() public promotion: Promotion;
  @Input() public loggedInUser: User;
  public pointsCollected = 0;
  public giftsToCollect = 0;

  constructor(private httpService: PromoqHttpService,
              private global: GlobalService) {
  }

  ngOnInit() {
    if (this.global.isUserInRole('ROLE_CLIENT')) {
      this.getPointsForClient();
      if (!(this.promotion.type === 0 && this.promotion.valuePromotionType === 1)) {
        this.getGiftsToCollect();
      }
    }
  }

  private getPointsForClient() {
    this.httpService.getPromotionPointsForLoggedInClient(this.promotion.id).then(
      (response: { amount: number }) => {
        this.pointsCollected = response.amount;
      }
    );
  }

  private getGiftsToCollect() {
    this.httpService.getGiftsToCollectForLoggedInClient(this.promotion.id).then(
      (response: { amount: number }) => {
        this.giftsToCollect = response.amount;
      }
    );
  }
}
