import {Component, Input} from '@angular/core';
import {Loading} from '../../../hooks';
import {User} from '../../../models/user';
import {Firm} from '../../../models/firm';
import {PromoqHttpService} from '../../../services/promoq-http.service';
import {Promotion} from '../../../models/promotion';
import {ClientAccountFirm} from "../../../models/client-account-firm";

@Component({
  selector: 'app-firm-promotions-panel',
  templateUrl: './firm-promotions-panel.component.html',
})
export class FirmPromotionsPanelComponent implements Loading {
  public loading: boolean;
  public title = 'Promotions';
  @Input() public loggedInUser: User;
  @Input() firm: Firm;
  public image: string | null;
  public promotions: Promotion[] = [];
  public clientAccountFirm: ClientAccountFirm = null;

  constructor(private httpService: PromoqHttpService) {
  }

  ngOnInit() {
    this.loading = true;
    this.getImage();
    this.getPromotions();
    this.getClientAccountFirm();
  }

  private getPromotions() {
    this.httpService.getAllActivePromotionsForFirm(this.firm.id).then(
      (response: Promotion[]) => {
        this.promotions = response;
        this.loading = false;
      });
  }

  private getImage() {
    const image = 'assets/img/firms/' + this.firm.id + '.png';
    this.httpService.checkFileExists(image).then(
      (result: boolean) => {
        this.image = result ? image : null;
      }
    );
  }

  private getClientAccountFirm() {
    this.httpService.getClientAccountFirmForLoggedInClient(this.firm.id).then(
      (response: ClientAccountFirm) => {
        this.clientAccountFirm = response;
      }
    );
  }
}
