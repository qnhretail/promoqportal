import {Component, OnInit} from '@angular/core';
import {GlobalService} from '../../services/global.service';
import {PromoqHttpService} from '../../services/promoq-http.service';
import {MatDialog} from '@angular/material';
import {User} from '../../models/user';
import {Subscription} from 'rxjs/Subscription';
import {NewClientAccountDialog} from '../../dialogs/new-client-account-dialog/new-client-account-dialog.component';
import {ClientAccount} from '../../models/account';
import {Client} from '../../models/client';
import {Page} from '../../hooks';
import {ClientDetailsDialog} from '../../dialogs/client-details-dialog/client-details-dialog.component';
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-clients-page',
  templateUrl: './clients-page.component.html',
})
export class ClientsPageComponent implements Page, OnInit {
  public loading: boolean;
  public title = 'Clients';
  public loggedInUserSubscription: Subscription;
  public loggedInUser: User;
  public clientAccounts: ClientAccount[] = [];

  constructor(private httpService: PromoqHttpService,
              private global: GlobalService,
              private dialog: MatDialog,
              private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.global.setBranding(this.route);
    this.loggedInUserSubscription = this.global.getUser()
      .subscribe((user: User) => {
        this.loading = true;
        this.loggedInUser = user;
        this.getClientAccounts();
      });
    this.global.checkRole(this.title);
  }

  ngOnDestroy() {
    this.loggedInUserSubscription.unsubscribe();
  }

  private getClientAccounts() {
    this.httpService.getAllClientAccounts().then(
      (result: ClientAccount[]) => {
        this.clientAccounts = result;
        this.loading = false;
      }, error => this.loading = false
    );
  }

  onResetPasswordClicked(clientAccount: ClientAccount) {
    this.global.warningDialogYesNo('Do you want to send a new password to ' + clientAccount.username + '?')
        .then(ok => this.resetPassword(clientAccount.id));
  }

  onAddClientAccountClicked() {
    this.dialog.open(NewClientAccountDialog, {
      data: {loggedInUser: this.loggedInUser}
    }).afterClosed().subscribe(
      (response: { account: ClientAccount, client: Client }) => {
        this.httpService.addClientAccount(response.account).then(
          (account: ClientAccount) => {
            response.client.email = account.email;
            response.client.accountId = account.id;
            this.httpService.addClient(response.client).then(
              (client: Client) => {
                this.clientAccounts.unshift(account);
                if (account.password != null) {
                  this.global.showPassword(account.password);
                }
              }
            );
          }
        );
      }
    );
  }


  private resetPassword(id: number) {
    this.httpService.resetPassword(id).then(
      (response: ClientAccount) => {
        if (response) {
          if (response.password != null) {
            this.showPassword(response.password);
          }
        }
      });
  }

  private showPassword(password) {
    this.global.showPassword(password);
  }

  public onDeleteClientAccountCliecked(clientAccount: ClientAccount) {
      this.global.warningDialogRemoval('account', clientAccount.username)
        .then(ok => {
          this.clientAccounts = this.clientAccounts.filter(x => x !== clientAccount);
          this.deleteClientAccount(clientAccount);
        });
  }

  private deleteClientAccount(clientAccount: ClientAccount, force: boolean = false) {
    this.httpService.deleteClientAccount(clientAccount.id, force).then(
      response => {
      },
      error => this.onRemoveClientAccountFailed(clientAccount, error, force)
    );
  }

  public checkClientAccounts() {
    if (this.clientAccounts === null) return false;
    return this.clientAccounts.length > 0;
  }

  public onClientAccountDetailsClicked(clientAccount: ClientAccount) {
    this.dialog.open(ClientDetailsDialog, {
      data: {loggedInUser: this.loggedInUser, clientAccount: clientAccount}
    }).afterClosed().subscribe(
      response => this.getClientAccounts()
    );
  }

  private onRemoveClientAccountFailed(clientAccount: ClientAccount, error: any, force: boolean) {
    this.clientAccounts.push(clientAccount);
    switch (error.status) {
      case 409:
        this.global.warningDialogNotEmpty('account', clientAccount.username)
          .then(ok => this.deleteClientAccount(clientAccount, true));
        break;
      default:
        this.global.warningDialogTryAgain('removing', 'account', clientAccount.username)
          .then(ok => this.deleteClientAccount(clientAccount, force));
        break;
    }
  }
}
