import {Injectable} from '@angular/core';

import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Branch} from '../models/branch';
import {StaffMember} from '../models/staff-member';
import {Product} from '../models/product';
import {Reward} from '../models/reward';
import {Promotion} from '../models/promotion';
import {Client} from '../models/client';
import {ClientAccount} from '../models/account';
import {SmtpServerConfiguration} from '../models/smtpServerConfiguration';
import {environment} from '../../environments/environment';
import {GlobalService} from './global.service';
import {ClientAccountFirm} from '../models/client-account-firm';
import {HttpError} from '../models/http-error';
import {Amount} from '../models/amount';
import {Register} from '../models/Register';

export enum ApiBranch {
  TICKETS = 9,
  GIFTARTICLES = 8,
  CLIENTS = 7,
  PROMOTIONS = 6,
  REWARDS = 5,
  PRODUCTS = 4,
  ACCOUNTS = 3,
  REGISTERS = 2,
  BRANCHES = 1,
  FIRMS = 0
}

@Injectable()
export class PromoqHttpService {
  private httpOptions = {withCredentials: true};
  private apiUri = environment.apiUrl;
  private apiUriRoot = this.apiUri + 'root/';
  private apiUriOwner = this.apiUri + 'owner/';
  private apiUriClient = this.apiUri + 'client/';
  private apiUriStaff = this.apiUri + 'staff/';
  private sessionUri = this.apiUri + 'session/';
  private firmsUriSuffix = 'firms/';
  private branchUriSuffix = 'branches/';
  private registerUriSuffix = 'registers/';
  private accountsUriSuffix = 'accounts/';
  private productsUriSuffix = 'products/';
  private rewardsUriSuffix = 'giftarticles/';
  private promotionsUriSuffix = 'promotions/';
  private clientsUriSuffix = 'clients/';
  private staffSuffix = 'staff/';
  private giftArticlesUriSuffix = 'giftarticles/';
  private ticketsUriSuffix = 'tickets/';

  constructor(private http: HttpClient,
              private global: GlobalService) {
  }

  private getUri(apiBranch: ApiBranch) {
    let uri: string;
    switch (this.global.getFirstRole()) {
      case 'ROLE_OWNER':
        uri = this.apiUriOwner;
        break;
      case 'ROLE_ROOT':
        uri = this.apiUriRoot;
        break;
      case 'ROLE_CLIENT':
        uri = this.apiUriClient;
        break;
      case 'ROLE_STAFF':
        uri = this.apiUriStaff;
        break;
    }
    switch (apiBranch) {
      case ApiBranch.BRANCHES:
        return uri + this.branchUriSuffix;
      case ApiBranch.FIRMS:
        return uri + this.firmsUriSuffix;
      case ApiBranch.REGISTERS:
        return uri + this.registerUriSuffix;
      case ApiBranch.ACCOUNTS:
        return uri + this.accountsUriSuffix;
      case ApiBranch.PRODUCTS:
        return uri + this.productsUriSuffix;
      case ApiBranch.REWARDS:
        return uri + this.rewardsUriSuffix;
      case ApiBranch.PROMOTIONS:
        return uri + this.promotionsUriSuffix;
      case ApiBranch.CLIENTS:
        return uri + this.clientsUriSuffix;
      case ApiBranch.GIFTARTICLES:
        return uri + this.giftArticlesUriSuffix;
      case ApiBranch.TICKETS:
        return uri + this.ticketsUriSuffix;
      default:
        return uri;
    }
  }

  public updateProduct(product: Product) {
    return this.httpPost(this.getProductsUri() + 'update/', product);
  }

  private httpGet(uri: string): Promise<Object> {
    if (this.global.isLoggedIn()) {
      return new Promise((resolve, reject) => {
        this.http.get(uri, this.httpOptions).subscribe(
          response => resolve(response),
          (error: HttpError) => {
            if (error.statusText === 'Not Found') {
              resolve(null);
            }
            reject(error);
          });
      });
    } else {
      return this.notLoggedIn();
    }
  }

  private notLoggedIn(): Promise<HttpError> {
    return new Promise((resolve, reject) => {
      const notLoggedInError = new HttpError();
      notLoggedInError.statusText = 'Not Logged In';
      reject(notLoggedInError);
    });
  }

  private httpPost(uri: string, body: any | null): Promise<Object> {
    if (this.global.isLoggedIn()) {
      return new Promise((resolve, reject) => {
        this.http.post(uri, body, this.httpOptions).subscribe(
          response => resolve(response),
          (error: HttpError) => {
            if (error.statusText === 'Not Found') {
              resolve(null);
            }
            reject(error);
          });
      });
    } else {
      return this.notLoggedIn();
    }
  }


  public login(authToken: string) {
    return new Promise((resolve, reject) => {
      this.http.post(this.sessionUri + 'login', {},
        {withCredentials: this.httpOptions.withCredentials, headers: new HttpHeaders().set('Authorization', authToken)})
        .subscribe(response => resolve(response), error => { reject(error); });
    });
  }

  public logOut() {
    const authToken: string = 'Basic ' + btoa('none' + ':' + 'none');
    return new Promise((resolve, reject) => {
      this.http.post(this.sessionUri + 'login', {},
        {withCredentials: this.httpOptions.withCredentials, headers: new HttpHeaders().set('Authorization', authToken)})
        .subscribe(response => resolve(response));
    });

  }

  public loginWithCookie() {
    return this.httpPost(this.sessionUri + 'login', null);
  }

  public getBranchUri() {
    return this.getUri(ApiBranch.BRANCHES);
  }

  public getBranchById(branchId: number) {
    return this.httpGet(this.getBranchUri() + branchId);
  }

  public addOrUpdateBranch(branch: Branch) {
    return this.httpPost(this.getBranchUri(), branch);
  }

  public deleteBranch(id: Number, force: boolean) {
    return this.httpDelete(this.getBranchUri() + id + '?force=' + force);
  }

  public getAllBranchesForFirm(id: number) {
    return this.httpGet(this.getBranchUri() + 'firm/' + id);
  }

  private getFirmsUri(): string {
    return this.getUri(ApiBranch.FIRMS);
  }

  public getAllFirms() {
    return this.httpGet(this.getFirmsUri());
  }

  public getAllFirmsWithPromotions() {
    return this.httpGet(this.getFirmsUri() + 'with_promotions');
  }

  public addFirm(firm) {
    return this.httpPost(this.getFirmsUri(), firm);
  }

  public deleteFirm(id: number) {
    return this.httpDelete(this.getFirmsUri() + id);
  }

  private getRegisterUri() {
    return this.getUri(ApiBranch.REGISTERS);
  }

  public getAllRegistersForBranch(id: number) {
    return this.httpGet(this.getRegisterUri() + 'branch/' + id);
  }

  public addRegister(register: Register) {
    return this.httpPost(this.getRegisterUri(), register);
  }

  public deleteRegister(id: number) {
      return this.httpDelete(this.getRegisterUri() + id);
  }

  private getAccountsUri() {
    return this.getUri(ApiBranch.ACCOUNTS);
  }

  public getAccount(id: number) {
    return this.httpGet(this.getAccountsUri() + id);
  }

  public resetPassword(id: number) {
    return this.httpGet(this.getAccountsUri() + 'resetPassword/' + id);
  }

  private getStaffAccountsUri() {
    return this.getAccountsUri() + this.staffSuffix;
  }

  public getStaffForBranch(branchId: number) {
        return this.httpGet(this.getStaffAccountsUri() + 'branch/' + branchId);
  }

  public addStaffMember(staffmember: StaffMember) {
    return this.httpPost(this.getStaffAccountsUri(), staffmember);
  }

  public addStaffMemberToBranch(id: number, branchId: number) {
    return new Promise((resolve, reject) => {
      this.http.put(this.getStaffAccountsUri() + 'branch/' + branchId + '/' + id, null, this.httpOptions)
        .subscribe(response => resolve(response), error => reject(error));
    });
  }

  public deleteStaffMemberFromBranch(id: number, branchId: number) {
      return this.httpDelete(this.getStaffAccountsUri() + 'branch/' + branchId + '/' + id);
  }

  public getAllStaffMemberFromFirm(firmId: number) {
    return this.httpGet(this.getStaffAccountsUri() + 'firm/' + firmId);
  }

  public getMaxFirms() {
    return this.httpGet(this.getAccountsUri() + 'maxfirms/');
  }

  public getAllClientAccounts() {
    return this.httpGet(this.getAccountsUri() + 'client/');
  }

  public getloggedInClientAccount() {
    return this.httpGet(this.getAccountsUri() + 'client/');
  }

  public setNewPassword(oldPassword: string, newPassword: string) {
    return this.httpPost(this.getAccountsUri() + 'client/setNewPassword/',
      {password: btoa(oldPassword + ':' + newPassword)});
  }

  public deleteClientAccount(id: number, force: boolean) {
    return this.httpDelete(this.getAccountsUri() + 'client/' + id + '?force=' + force);
  }

  public addClientAccount(account: ClientAccount) {
    return this.httpPost(this.getAccountsUri() + 'client/', account);
  }

  public updateClientAccount(account: ClientAccount) {
    return this.httpPost(this.getAccountsUri() + 'client/update/', account);
  }

  public getProductsUri() {
    return this.getUri(ApiBranch.PRODUCTS);
  }

  public addProduct(product: Product) {
    return this.httpPost(this.getProductsUri(), product);
  }

  private httpDelete(uri: string): Promise<Object> {
    if (this.global.isLoggedIn()) {
      return new Promise((resolve, reject) => {
        this.http.delete(uri, this.httpOptions).subscribe(response => null,
          (error: HttpError) => {
            if (error.status !== 200) reject(error);
          });
      });
    } else {
      return this.notLoggedIn();
    }
  }

  public getAllProductsForFirm(id: number) {
    return this.httpGet(this.getProductsUri() + 'firm/' + id);
  }

  public getRewardsUri() {
    return this.getUri(ApiBranch.REWARDS);
  }

  public getAllRewardsForFirm(firmId: number) {
    return this.httpGet(this.getRewardsUri() + 'firm/' + firmId);
  }

  public addReward(reward: Reward) {
    return this.httpPost(this.getRewardsUri(), reward);
  }

  public getPromotionsUri() {
    return this.getUri(ApiBranch.PROMOTIONS);
  }

  public getPromotion(id: number) {
    return this.httpGet(this.getPromotionsUri() + id);
  }

  public getAllPromotionsFromFirm(firmId: number) {
    return this.httpGet(this.getPromotionsUri() + 'firm/' + firmId);
  }

  public getAllActivePromotionsForFirm(firmId: number) {
    return this.httpGet(this.getPromotionsUri() + 'firm/' + firmId + '?active=true');
  }

  public addPromotion(promotion: Promotion) {
    return this.httpPost(this.getPromotionsUri(), promotion);
  }

  public deletePromotion(id: number) {
    return this.httpDelete(this.getPromotionsUri() + id);
  }

  public addPromotionPointsToClientAccount(clientAccountId: number,
                                           promotionId: number,
                                           amount: Amount) {
    return this.httpPost(this.getPromotionsUri() + 'points/' + promotionId + '/' + clientAccountId,
      amount);
  }

  public getPromotionPointsForLoggedInClient(id: number) {
    return this.httpGet(this.getPromotionsUri() + 'points/' + id);
  }

  public getPromotionPointsForClient(promotionId: number, clientAccountId: number) {
    return this.httpGet(this.getPromotionsUri() + 'points/' + promotionId + '/' + clientAccountId);
  }

  public getPointTransferHistoryForClient(id: number) {
    return this.httpGet(this.getPromotionsUri() + 'pointtransfers/client/' + id);
  }

  public getCreditTransferHistoryForClient(id: number) {
    return this.httpGet(this.getPromotionsUri() + 'credittransfers/client/' + id);
  }

  public getClientsUri() {
    return this.getUri(ApiBranch.CLIENTS);
  }

  public addClient(client: Client) {
    return this.httpPost(this.getClientsUri(), client);
  }

  public getAllClients() {
    return this.httpGet(this.getClientsUri());
  }

  public getClientByClientCardId(id: string) {
    return this.httpGet(this.getClientsUri() + id);
  }

  public deleteClient(id: number) {
    return this.httpDelete(this.getClientsUri() + id);
  }

  public getAllClientsForClientAccount(id: number) {
    return this.httpGet(this.getClientsUri() + 'clientAccount/' + id);
  }

  public getAllClientAccountFirmsForClient(id: number) {
    return this.httpGet(this.getClientsUri() + 'clientAccountFirms/' + id);
  }
  public getClientAccountFirm(clientAccountId: number, firmId: number) {
    return this.httpGet(this.getClientsUri() + 'clientAccountFirms/' + clientAccountId + '/' + firmId);
  }

  public updateClientAccountFirm(clientAccountFirm: ClientAccountFirm) {
    return this.httpPost(this.getClientsUri() + 'clientAccountFirms/', clientAccountFirm);
  }

  public addCreditToClientAccountFirm(clientAccountId, firmId, amount: Amount) {
    return this.httpPost(this.getClientsUri() + 'clientAccountFirms/addCredit/' + clientAccountId
      + '/' + firmId, amount);
  }

  public getClientAccountFirmForLoggedInClient(firmId: number) {
    return this.httpGet(this.getClientsUri() + 'clientAccountFirms/loggedInClient/' + firmId);
  }

  public checkFileExists(filename) {
    return new Promise((resolve, rejecte) => this.http.get(filename).subscribe(
      response => resolve(true), error => resolve(error.status === 200)
    ));
  }

  private getGiftArticlesUri() {
    return this.getUri(ApiBranch.GIFTARTICLES);
  }

  public getGiftsToCollectForLoggedInClient(id: number) {
    return this.httpGet(this.getGiftArticlesUri() + 'amount/' + id);
  }

  public getGiftsToCollectForClientAccount(promotionId: number, clientAccountId: number) {
    return this.httpGet(this.getGiftArticlesUri() + 'amount/' + promotionId + '/' + clientAccountId);
  }

  public saveSmtpServerConfiguration(smtpServerConfiguration: SmtpServerConfiguration) {
    return this.httpPost( this.getAccountsUri() + 'configuration/smtpserver/', smtpServerConfiguration);
  }

  public getSmtpServerConfiguration() {
    return this.httpGet( this.getAccountsUri() + 'configuration/smtpserver/');
  }

  public updateSmtpServerPassword(password: string) {
    return this.httpPost(this.getAccountsUri() + 'configuration/smtpserver/password/', password);
  }

  private getTicketsUri() {
    return this.getUri(ApiBranch.TICKETS);
  }

  public getTicketHistoryForClient(id: number) {
    return this.httpGet(this.getTicketsUri() + 'history/client/' + id);
  }

  public getTicketProductsForTicket(id: number) {
    return this.httpGet(this.getTicketsUri() + id + '/ticketProducts');
  }

}
