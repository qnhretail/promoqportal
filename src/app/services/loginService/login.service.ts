import {Injectable} from "@angular/core";
import {MatDialog} from "@angular/material";
import {LoginDialog} from "../../dialogs/login-dialog/login-dialog.component";

@Injectable(
)
export class LoginService {
  result;

  constructor(public dialog: MatDialog) {
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(LoginDialog, {
      data: {}
    });

    dialogRef.afterClosed().subscribe(result => {
      this.result = result;
    });
  }
}

