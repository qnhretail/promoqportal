import {Injectable, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {User} from '../models/user';
import {Menu} from '../models/menu';
import {ActivatedRoute, Router} from '@angular/router';
import {isUndefined} from 'util';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {MatDialog, MatDialogRef} from '@angular/material';
import {WarningDialog} from '../dialogs/warning-dialog/warning-dialog.component';
import {TranslateService} from '@ngx-translate/core';
import {environment} from '../../environments/environment';
import {PasswordDialog} from '../dialogs/password-dialog/password-dialog.component';

@Injectable()
export class GlobalService implements OnInit {
  public companyLogo = environment.logoSrc;
  public noUser = new User('NONE', [{name: 'ROLE_NONE'}]);
  public loggedInUser: BehaviorSubject<User>;
  public company: string;
  public currentMenu: Menu;

  constructor(private router: Router,
              private dialog: MatDialog,
              private translate: TranslateService) {
  }

  ngOnInit(): void {
  }

  init() {
    this.loggedInUser = new BehaviorSubject(this.noUser);
  }

  public setUser(user: User): void {
    this.loggedInUser.next(user);
  }

  public getUser(): Observable<User> {
    return this.loggedInUser.asObservable();
  }

  clearLogedinUser(): void {
    this.loggedInUser.next(this.noUser);
  }

  checkRole(title: string): void {
    let x;
    if (isUndefined(this.currentMenu)) x = false;
    else {
      x = this.currentMenu.menu.some(
        menuItem => {
          return menuItem.title === title;
        });
    }
    if (!x) this.navigateHome();
  }

  public navigateHome() {
    this.router.navigateByUrl(this.company);
  }

  public warningDialogRemoval(type: string, id): Promise<any> {

    return this.warningDialogYesNo(this.translate.instant('Are you sure you want to remove {{type}} {{id}}?',
      {type: this.translate.instant(type), id: id}));
  }

  public warningDialogIfClose(original: any, changed: any, dialogRef: MatDialogRef<any>): void {
    if (JSON.stringify(original) !== JSON.stringify(changed)) {
      this.warningDialogYesNo(this.translate.instant('Are you sure you want to exit without saving?')).then(
        ok => dialogRef.close()
      );
    } else {
      dialogRef.close();
    }
  }

  public warningDialogTryAgain(task, type: string, id: (string | number)): Promise<any> {
    return this.warningDialogYesNo(this.translate.instant('There was an error {{task}} {{type}} {{id}}, would you like to try again?',
      {task: this.translate.instant(task), type: this.translate.instant(type), id: id}));
  }


  public warningDialogYesNo(message: string): Promise<any> {
    return this.warningDialog(message, true);
  }

  public warningDialog(message: string, yesNo: boolean): Promise<any> {
    return new Promise((resolve, reject) => {
      this.dialog.open(WarningDialog, {
        data: {message: message, yesNo: yesNo}
      }).afterClosed().subscribe(
        response => {
          if (response === true) {
            resolve();
          }
        }
      );
    });
  }

  public warningDialogOneButton(message: string): Promise<any> {
    return this.warningDialog(message, false);
  }

  public showPassword(password: string) {
    this.dialog.open(PasswordDialog , {
      data: {password: password}
    });
  }

  warningDialogNotEmpty(type: string, id: string) {
    return this.warningDialogYesNo((this.translate.instant('The selected {{type}}, {{id}}, is not empty, do you want to force the removal?',
      {type: this.translate.instant(type), id: id})));
  }

  isLoggedIn() {
    return !this.isUserInRole('ROLE_NONE');
  }

  isUserInRole(role: string): boolean {
    const userRoles: any[] = this.loggedInUser.getValue().roles;
    return userRoles.some(userRole => userRole.name === role);
  }

  getFirstRole(): string {
    return this.loggedInUser.getValue().roles[0].name;
  }

  clone(object: any): any {
    return Object.assign({}, object);
  }

  setBranding(route: ActivatedRoute) {
    this.company = route.snapshot.paramMap.get('companyName');
    if ((this.company === null || this.company === 'null') &&
      this.loggedInUser.getValue().username !== 'NONE') {
      this.company = this.loggedInUser.getValue().username;
    }
    switch (this.company) {
      case ('wouters'):
        document.body.style.backgroundImage = environment.wouters.bodyBg;
        this.companyLogo = environment.wouters.logoSrc;
        break;
      default:
        document.body.style.backgroundImage = environment.bodyBg;
        this.companyLogo = environment.logoSrc;
    }
  }
}
