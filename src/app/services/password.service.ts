import {Injectable} from '@angular/core';

@Injectable()
export class PasswordService {

  constructor() {
  }

  public validatePassword(password: string): boolean {
    // If password is at least 8 characters long, has at least 1 digit and 1 non-alphanumeric character return true
    if (/^\s*(?=[\S]{8,})(?=(\S*\d+\S*)+)(?=(\S*[^\wÀ-úÀ-ÿ\s]+\S*)+)\s*/.test(password)) {
      return true;
    }
    return false;
  }

}
