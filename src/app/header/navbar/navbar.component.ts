import {Component, HostListener, OnInit} from '@angular/core';
import {GlobalService} from '../../services/global.service';
import {LoginService} from '../../services/loginService/login.service';
import {PromoqHttpService} from '../../services/promoq-http.service';
import {CookieService} from 'ngx-cookie-service';
import {HttpClient} from '@angular/common/http';
import {Menu} from '../../models/menu';
import {User} from '../../models/user';
import {Subscription} from 'rxjs';
import {ClientDetailsDialog} from '../../dialogs/client-details-dialog/client-details-dialog.component';
import {ClientAccount} from '../../models/account';
import {MatDialog} from '@angular/material';
import {Router} from '@angular/router';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {environment} from '../../../environments/environment';
import {window} from "rxjs/operator/window";

@Component({
  selector: 'app-navbar',
  templateUrl: 'navbar.component.html',
  styleUrls: ['navbar.component.css'],
  providers: [LoginService, PromoqHttpService],
  animations: [
    trigger('menuSlideDown', [
      state('auto', style({
        height: 'auto'
      })),
      state('in', style({
        height: '9px'
      })),
      state('out', style({
        height: '51px'
      })),
      transition('in => out', animate('400ms ease-in-out')),
      transition('out => in', animate('400ms ease-in-out'))
    ]),
  ]
})
export class NavbarComponent implements OnInit {
  menuState = 'in';
  public showTopNav = false;
  private menus: Menu[];
  private loggedInUserSubscription: Subscription;
  @HostListener('window:resize') onResize() {
    this.setMenu();
  }

  constructor(public global: GlobalService,
              private loginService: LoginService,
              private cookieService: CookieService,
              private http: HttpClient,
              private dialog: MatDialog,
              private httpService: PromoqHttpService,
              private router: Router) {
    this.http.get('./assets/menu.json').subscribe(
      result => {
        this.menus = Object.values(result);
        this.loggedInUserSubscription = this.global.getUser()
          .subscribe(user => {
            this.loggedInUser = user;
            this.setMenu();
          });
      });
  }

  private loggedInUser: User = this.global.noUser;

  openLoginDialog(): void {
    this.loginService.openDialog();
  }

  onLogoutClicked() {
    this.cookieService.delete('JSESSIONID', '/', 'localhost');
    this.httpService.logOut().then(response => {
      this.global.clearLogedinUser();
    });
    this.global.clearLogedinUser();
  }

  ngOnInit() {
  }

  public onAccountDetailsClicked() {
    if (this.global.isUserInRole('ROLE_CLIENT')) {
      this.httpService.getloggedInClientAccount().then(
        (clientAccount: ClientAccount) => {
          this.dialog.open(ClientDetailsDialog, {
            data: {loggedInUser: this.loggedInUser, clientAccount: clientAccount}
          });
        }
      );
    }
  }

  private setMenu() {
    const role = this.global.getFirstRole();
    this.global.currentMenu = null;
    this.menus.some(menu => {
        if (menu.role === role) {
          this.global.currentMenu = menu;
          return true;
        }
      }
    );
    const width = screen.width;
    if (width <= environment.bootstrap_xs ) {
      this.menuState = 'auto';
      this.showTopNav = false;
    } else {
      this.menuState = this.global.currentMenu !== null ? 'out' : 'in';
    }
  }
  toggleTopNavCollapse() {
    this.showTopNav = !this.showTopNav;
  }
}
