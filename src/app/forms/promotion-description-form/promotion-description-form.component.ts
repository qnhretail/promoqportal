import {Component, Input, OnInit} from '@angular/core';
import {Promotion} from '../../models/promotion';

@Component({
  selector: 'app-promotion-description-form',
  templateUrl: './promotion-description-form.component.html',
})
export class PromotionDescriptionFormComponent implements OnInit {
  @Input() promotion: Promotion;

  constructor() {
  }

  ngOnInit() {
  }

}
