import {Component, Input} from '@angular/core';
import {Loading} from '../../hooks';
import {User} from '../../models/user';
import {ClientAccount} from '../../models/account';
import {MatDialog} from '@angular/material';
import {GlobalService} from '../../services/global.service';
import {PromoqHttpService} from '../../services/promoq-http.service';
import {Client} from '../../models/client';
import {NewClientDialog} from '../../dialogs/new-client-dialog/new-client-dialog.component';
import {isUndefined} from 'util';

@Component({
  selector: 'app-client-clientcards',
  templateUrl: './client-clientcards.component.html',
})
export class ClientClientcardsComponent implements Loading {
  @Input() clients: Client[];
  @Input() loggedInUser: User;
  @Input() clientAccount: ClientAccount;
  public loading: boolean;

  constructor(private dialog: MatDialog,
              private global: GlobalService,
              private httpService: PromoqHttpService) {
  }

  ngOnInit() {
    this.loading = true;
    this.getClients();
  }

  private getClients() {
    this.httpService.getAllClientsForClientAccount(this.clientAccount.id).then(
      (response: Client[]) => {
        this.clients = response;
        this.loading = false;
      }, error => this.loading = false
    );
  }

  public onDeleteClientClicked(id: number) {
    if (this.global.isLoggedIn()) {
      this.global.warningDialogRemoval('client', id)
        .then(ok => {
          const client = this.clients.find(x => x.id === id);
          this.clients = this.clients.filter(x => x !== client);
          this.deleteClient(client);
        });
    }
  }

  private deleteClient(client: Client) {
    this.httpService.deleteClient(client.id).then(
      response => {
      },
      error => this.onDeleteClientFailed(client)
    );
  }

  private onDeleteClientFailed(client: Client) {
    this.clients.unshift(client);
    this.global.warningDialogTryAgain('removing', 'clientcard', client.cardId)
      .then(ok => this.deleteClient(client));
  }

  private onAddClientClicked() {
    this.dialog.open(NewClientDialog, {
      data: {loggedInUser: this.loggedInUser, client: null}
    }).afterClosed().subscribe(
      (response: { client: Client }) => {
        if (!isUndefined(response)) {
          response.client.accountId = this.clientAccount.id;
          this.addClient(response.client);
        }
      }
    );
  }

  private addClient(client: Client) {
    this.httpService.addClient(client).then(
      (response: Client) => this.clients.unshift(response),
      error => this.onAddClientFailed(client)
    );
  }

  private onAddClientFailed(client: Client) {
    this.global.warningDialogTryAgain('adding', 'clientcard', client.cardId)
      .then(ok => this.addClient(client));
  }

}
