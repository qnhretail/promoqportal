import {Component, Input, OnInit} from '@angular/core';
import {Promotion} from '../../models/promotion';
import {Reward} from '../../models/reward';
import {PromoType} from '../../models/promo-type';
import {MatDialog} from '@angular/material';
import {NewRewardDialog} from '../../dialogs/new-reward-dialog/new-reward-dialog.component';
import {User} from '../../models/user';
import {Firm} from '../../models/firm';
import {isUndefined} from 'util';
import {PromoqHttpService} from '../../services/promoq-http.service';
import {GlobalService} from '../../services/global.service';
import {PromoTypes} from '../../promo-types';

@Component({
  selector: 'app-promotion-configuration-form',
  templateUrl: './promotion-configuration-form.component.html',
})
export class PromotionConfigurationFormComponent implements OnInit {
  @Input() promotion: Promotion;
  @Input() loggedInUser: User;
  @Input() firm: Firm;

  rewards: Reward[];
  public valuePromoTypes: PromoType[];
  public promoTypes: PromoType[];

  constructor(private global: GlobalService,
              private httpService: PromoqHttpService,
              private dialog: MatDialog) {
  }

  ngOnInit() {
    this.promoTypes = PromoTypes.promoTypes;
    this.valuePromoTypes = PromoTypes.valuePromoTypes;
    this.getAllRewardsForFirm();
  }

  getAllRewardsForFirm(): void {
    this.httpService.getAllRewardsForFirm(this.firm.id).then(
      (response: Reward[]) => this.rewards = response
    );
  }

  onAddNewRewardClicked(): void {
    this.dialog.open(NewRewardDialog,
      {
        data: {
          firm: this.firm,
          loggedInUser: this.loggedInUser
        }
      }).afterClosed().subscribe(
      (response: Reward) => {
        if (!isUndefined(response)) {
          this.addReward(response);
        }
      }
    );
  }

  addReward(reward: Reward): void {
    this.httpService.addReward(reward).then(
      (response: Reward) => this.rewards.unshift(response),
      error => this.onAddRewardFailed(reward)
    );
  }

  onAddRewardFailed(reward: Reward): void {
    this.global.warningDialogTryAgain('adding', 'reward', Reward.name)
      .then(ok => this.addReward(reward));
  }
}
