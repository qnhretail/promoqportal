import {Component, Input, OnInit} from '@angular/core';
import {Firm} from '../../models/firm';
import {User} from '../../models/user';
import {Promotion} from '../../models/promotion';
import {Branch} from '../../models/branch';
import {PromoqHttpService} from '../../services/promoq-http.service';

@Component({
  selector: 'app-promotion-branches-form',
  templateUrl: './promotion-branches-form.component.html',
})
export class PromotionBranchesFormComponent implements OnInit {
  @Input() promotion: Promotion;
  @Input() data: { firm: Firm, loggedInUser: User };
  public branches: Branch[] = [];
  public selectedBranch: number | null = null;

  constructor(private httpService: PromoqHttpService) {
  }

  ngOnInit() {
    this.getBranches();
  }

  getBranches() {
    this.httpService.getAllBranchesForFirm(this.data.firm.id).then(
      (response: Branch[]) => this.branches = response, error => null
    );
  }

  onAddBranchClicked() {
    if (this.promotion.branches.indexOf(this.selectedBranch) === -1) {
      this.promotion.branches.push(this.selectedBranch);
    }
  }

  onDeleteBranchClicked(id: number) {
    this.promotion.branches = this.promotion.branches.filter(x => x !== id);
  }

  getBranchNameForId(id: number) {
    return this.branches.find(x => x.id === id).name;
  }
}
