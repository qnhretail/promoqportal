import {Component, Input, OnInit} from '@angular/core';
import {Promotion} from '../../models/promotion';
import {User} from '../../models/user';
import {Firm} from '../../models/firm';
import {Reward} from '../../models/reward';
import {PromoType} from '../../models/promo-type';
import {GlobalService} from '../../services/global.service';
import {PromoqHttpService} from '../../services/promoq-http.service';
import {MatDialog} from '@angular/material';
import {PromoTypes} from '../../promo-types';
import {Branch} from '../../models/branch';
import {branch} from '@angular-devkit/schematics/src/tree/static';

@Component({
  selector: 'app-promotion-details-configuration',
  templateUrl: './promotion-details-configuration.component.html',
})
export class PromotionDetailsConfigurationComponent implements OnInit {
  @Input() promotion: Promotion;
  @Input() loggedInUser: User;
  @Input() firm: Firm;

  rewards: Reward[];
  public valuePromoTypes: PromoType[];
  public promoTypes: PromoType[];
  private branches: Branch[];

  constructor(private global: GlobalService,
              private httpService: PromoqHttpService,
              private dialog: MatDialog) {
  }

  ngOnInit() {
    this.promoTypes = PromoTypes.promoTypes;
    this.valuePromoTypes = PromoTypes.valuePromoTypes;
    this.getAllRewardsForFirm();
    if (!this.promotion.firmPromotion) {
      this.getBranches();
    }
  }

  getAllRewardsForFirm(): void {
    this.httpService.getAllRewardsForFirm(this.firm.id).then(
      (response: Reward[]) => this.rewards = response
    );
  }

  public getPromotionType(): string {
    return this.promoTypes.find(promoType =>
      promoType.id === this.promotion.type).name;
  }

  public getValuePromotionType(): string {
    return this.valuePromoTypes.find(valuePromoType =>
      valuePromoType.id === this.promotion.valuePromotionType).name;
  }

  public getReward(): string {
    return this.rewards.find(reward =>
      reward.id === this.promotion.giftArticleId).name;
  }

  public getBranchString(id: number): string {
    const thisBranch = this.branches.find(x => x.id === id);
    return thisBranch.name + ': ' + thisBranch.address;
  }

  private getBranches() {
    this.httpService.getAllBranchesForFirm(this.firm.id).then(
      (response: Branch[]) => this.branches = response, error => null
    );
  }
}
