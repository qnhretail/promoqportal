import {Component, Input, OnInit} from '@angular/core';
import {Ticket} from '../../models/ticket';
import {PromoqHttpService} from '../../services/promoq-http.service';
import {TicketProduct} from '../../models/ticket-product';
import {TicketProductGroup} from '../../models/ticket-product-group';

@Component({
  selector: 'app-ticket-details-product-list',
  templateUrl: './ticket-details-product-list.component.html',
})
export class TicketDetailsProductListComponent implements OnInit {
  public ticketProducts: TicketProduct[] = [];
  public groupedTicketProducts: TicketProductGroup[] = [];
  @Input() public ticket: Ticket;

  constructor(private httpService: PromoqHttpService) { }

  ngOnInit() {
    this.getProducts();
  }

  private getProducts() {
    this.httpService.getTicketProductsForTicket(this.ticket.id).then(
      (ticketProducts: TicketProduct[]) => {
        this.ticketProducts = ticketProducts;
        this.groupTicketProducts();
      }
    );
  }

  groupTicketProducts() {
    this.ticketProducts.forEach(ticketProduct => {
      let group = this.groupedTicketProducts.find(x => x.plu === ticketProduct.plu);
      if ( typeof group !== 'undefined' ) {
        group.ticketProducts.push(ticketProduct);
      } else {
        group = new TicketProductGroup(ticketProduct);
        group.ticketProducts.push(ticketProduct);
        this.groupedTicketProducts.push(group);
      }
    });
  }


  getTotalPrice(inclDiscount: Boolean) {
    let totalPrice = 0;
    if (inclDiscount === true) {
      this.ticketProducts.forEach(tickerPruduct => totalPrice += tickerPruduct.priceInclDiscount);
    } else {
      this.ticketProducts.forEach(tickerPruduct => totalPrice += tickerPruduct.price);
    }
    return totalPrice;
  }
}
