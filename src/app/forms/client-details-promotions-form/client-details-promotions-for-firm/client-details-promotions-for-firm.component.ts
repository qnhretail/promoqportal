import {Component, Input, OnInit} from '@angular/core';
import {Firm} from '../../../models/firm';
import {PromoqHttpService} from '../../../services/promoq-http.service';
import {Promotion} from '../../../models/promotion';
import {ClientAccountFirm} from '../../../models/client-account-firm';
import {AssignPointsToClientDialog} from "../../../dialogs/assign-points-to-client-dialog/assign-points-to-client-dialog.component";
import {GlobalService} from "../../../services/global.service";
import {MatDialog} from "@angular/material";
import {isDefined} from "@ngx-translate/core/src/util";
import {Amount} from "../../../models/amount";

@Component({
  selector: 'app-client-details-promotions-for-firm',
  templateUrl: './client-details-promotions-for-firm.component.html',
})
export class ClientDetailsPromotionsForFirmComponent implements OnInit {
  @Input() firm: Firm;
  @Input() clientAccountId: number;
  public promotions: Promotion[];
  public clientAccountFirm: ClientAccountFirm;

  constructor(private httpService: PromoqHttpService,
              private dialog: MatDialog,
              public global: GlobalService) { }

  ngOnInit() {
    this.getPromotions();
    this.getClientAccountFirm();
  }

  private getPromotions() {
    this.httpService.getAllPromotionsFromFirm(this.firm.id).then(
      (promotions: Promotion[]) => {
        this.promotions = promotions;
      }
    );
  }

  private getClientAccountFirm() {
    this.httpService.getClientAccountFirm(this.clientAccountId, this.firm.id).then(
      (response: ClientAccountFirm) => {
        this.clientAccountFirm = response;
      }
    );
  }

  public onAssignCreditClicked() {
    this.dialog.open(AssignPointsToClientDialog, {
      data: {clientAccountId: this.clientAccountId, promotionId: null}
    }).afterClosed().subscribe(
      (response: Amount) => {
        if (isDefined(response.amount)) {
          this.httpService.addCreditToClientAccountFirm(this.clientAccountId, this.clientAccountFirm.firmId, response).then(
            (clientAccountfirm: ClientAccountFirm) => this.clientAccountFirm = clientAccountfirm
          );
        }
      }
    );
  }
}
