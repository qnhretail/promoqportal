import {Component, Input, OnInit} from '@angular/core';
import {Promotion} from '../../../../models/promotion';
import {PromoqHttpService} from '../../../../services/promoq-http.service';
import {MatDialog} from '@angular/material';
import {AssignPointsToClientDialog} from '../../../../dialogs/assign-points-to-client-dialog/assign-points-to-client-dialog.component';
import {isDefined} from '@ngx-translate/core/src/util';
import {GlobalService} from '../../../../services/global.service';
import {Amount} from '../../../../models/amount';

@Component({
  selector: '[app-client-details-promotion]',
  templateUrl: './client-details-promotion.component.html',
})
export class ClientDetailsPromotionComponent implements OnInit {
  giftArticlesToCollect: { amount: number; } = {amount: 0};
  collectedPoints: { amount: number; } = {amount: 0};
  @Input() promotion: Promotion;
  @Input() clientAccountId: number;

  constructor( private httpService: PromoqHttpService,
               private dialog: MatDialog,
               public global: GlobalService) { }

  ngOnInit() {
    this.getPromotionPointsForClientAccount();
    if (!(this.promotion.type === 0 && this.promotion.valuePromotionType === 1)) {
      this.getGiftsToCollectForClientAccount();
    }
  }

  public getPromotionPointsForClientAccount() {
    this.httpService.getPromotionPointsForClient(this.promotion.id, this.clientAccountId).then(
      (collectedPoints: {amount: number}) => this.collectedPoints = collectedPoints
    );
  }

  public onAssignPointsClicked() {
    this.dialog.open(AssignPointsToClientDialog, {
      data: {clientAccountId: this.clientAccountId, promotionId: this.promotion.id}
    }).afterClosed().subscribe(
      (response: Amount) => {
        if (isDefined(response.amount)) {
          this.httpService.addPromotionPointsToClientAccount(
            this.clientAccountId, this.promotion.id, {amount: response.amount, reason: response.reason})
            .then(
              (collectedPoints: { amount: number}) => {
                this.collectedPoints = collectedPoints;
                this.getGiftsToCollectForClientAccount();
              },
              error => {}
            );
        }
      }
    );
  }

  private getGiftsToCollectForClientAccount() {
    this.httpService.getGiftsToCollectForClientAccount(this.promotion.id, this.clientAccountId).then(
      (giftArticlesToCollect: { amount: number }) => this.giftArticlesToCollect = giftArticlesToCollect
    );
  }
}
