import {Component, Input, OnInit} from '@angular/core';
import {ClientAccount} from '../../models/account';
import {PromoqHttpService} from '../../services/promoq-http.service';
import {Firm} from '../../models/firm';

@Component({
  selector: 'app-client-details-promotions-form',
  templateUrl: './client-details-promotions-form.component.html',
})
export class ClientDetailsPromotionsFormComponent implements OnInit {
  @Input() clientAccount: ClientAccount;
  public firms: Firm[];

  constructor(private httpService: PromoqHttpService) { }

  ngOnInit() {
    this.getFirms();
  }

  private getFirms() {
    this.httpService.getAllFirms().then(
      (firms: Firm[]) => {
        this.firms = firms;
      }
    );
  }
}
