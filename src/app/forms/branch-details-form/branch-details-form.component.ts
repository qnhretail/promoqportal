import {Component, Input, OnInit} from '@angular/core';
import {Branch} from '../../models/branch';
import {User} from '../../models/user';

@Component({
  selector: 'app-branch-details-form',
  templateUrl: './branch-details-form.component.html',
})
export class BranchDetailsFormComponent implements OnInit {
  @Input() branch: Branch;

  constructor() { }

  ngOnInit() {
  }
}
