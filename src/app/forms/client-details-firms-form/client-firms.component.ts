import {Component, Input} from '@angular/core';
import {Loading} from '../../hooks';
import {User} from '../../models/user';
import {ClientAccount} from '../../models/account';
import {GlobalService} from '../../services/global.service';
import {PromoqHttpService} from '../../services/promoq-http.service';
import {ClientAccountFirm} from '../../models/client-account-firm';

@Component({
  selector: 'app-client-firms',
  templateUrl: './client-firms.component.html',
})
export class ClientFirmsComponent implements Loading {
  public loading: boolean;
  public firms: ClientAccountFirm[] = [];
  @Input() loggedInUser: User;
  @Input() clientAccount: ClientAccount;

  constructor(private httpService: PromoqHttpService,
              public global: GlobalService) {
  }

  ngOnInit() {
    this.loading = true;
    this.getClientAccountFirms();
  }

  private getClientAccountFirms() {
    this.httpService.getAllClientAccountFirmsForClient(this.clientAccount.id).then(
      (response: ClientAccountFirm[]) => {
        this.firms = response;
        this.firms.forEach(firm => {
          firm.receiveNewsletterOrig = firm.receiveNewsletter;
          firm.receivePromotionsOrig = firm.receivePromotions;
        });
        this.loading = false;
      }, error => this.loading = false
    );
  }

  private onSaveChangesClicked(clientAccountFirm: ClientAccountFirm) {
    this.httpService.updateClientAccountFirm(clientAccountFirm);
  }

  public isClientAccountFirmChamged(firm: ClientAccountFirm): boolean {
    return (firm.receiveNewsletter !== firm.receiveNewsletterOrig ||
      firm.receivePromotions !== firm.receivePromotionsOrig);
  }
}
