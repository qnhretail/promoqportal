import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ClientAccount} from "../../models/account";
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {GlobalService} from "../../services/global.service";

@Component({
  selector: 'app-new-account-form',
  templateUrl: './new-account-form.component.html',
})
export class NewAccountFormComponent implements OnInit {
  accountForm: FormGroup;
  @Input() account: ClientAccount;
  @Input() columns: number;
  @Output() valid: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor(builder: FormBuilder,
              public global: GlobalService) {
    this.accountForm = builder.group({});
  }

  ngOnInit() {
    this.accountForm.addControl('username', new FormControl(this.account.username, [
      Validators.required, Validators.minLength(6)]));
    this.accountForm.addControl('email', new FormControl(this.account.email, []));
    this.accountForm.addControl('phone', new FormControl(this.account.phone, []));
    this.accountForm.addControl('address', new FormControl(this.account.address, []));
    this.accountForm.addControl('zipCode', new FormControl(this.account.zipCode, []));
    this.accountForm.addControl('town', new FormControl(this.account.town, []));
    this.accountForm.addControl('note', new FormControl(this.account.note, []));
    this.valid.next(false);
    this.accountForm.valueChanges.subscribe(form => this.valid.next(this.accountForm.valid));
  }

  logAccount() {
  }

  get email() {
    return this.accountForm.get('email');
  }

  get username() {
    return this.accountForm.get('username');
  }

}
