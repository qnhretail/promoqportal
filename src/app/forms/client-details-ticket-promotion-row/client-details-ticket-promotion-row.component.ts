import {Component, Input, OnInit} from '@angular/core';
import {Promotion} from '../../models/promotion';
import {TicketPromotion} from '../../models/ticket-promotion';
import {PromoqHttpService} from "../../services/promoq-http.service";

@Component({
  selector: '[app-client-details-ticket-promotion-row]',
  templateUrl: './client-details-ticket-promotion-row.component.html',
})
export class ClientDetailsTicketPromotionRowComponent implements OnInit {
  @Input() public promotion: TicketPromotion;
  public promotionDetails: Promotion = new Promotion;

  constructor(private httpService: PromoqHttpService) { }

  ngOnInit() {
    this.getPromotionDetails();
  }

  private getPromotionDetails() {
    this.httpService.getPromotion(this.promotion.promotionId).then(
      (promotionDetails: Promotion) => this.promotionDetails = promotionDetails
    );
  }
}
