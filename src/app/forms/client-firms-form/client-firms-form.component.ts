import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {PromoqHttpService} from '../../services/promoq-http.service';
import {User} from '../../models/user';
import {Firm} from '../../models/firm';
import {ClientAccount} from '../../models/account';

@Component({
  selector: 'app-client-firms-form',
  templateUrl: './client-firms-form.component.html',
})
export class ClientFirmsFormComponent implements OnInit {
  @Input() account: ClientAccount;
  @Input() loggedInUser: User;
  @Output() valid: EventEmitter<Boolean> = new EventEmitter<Boolean>();
  public firms: Firm[] = [];
  public selectedFirm: number | null = null;

  constructor(private httpService: PromoqHttpService) {
  }

  ngOnInit() {
    this.getAllFirms();
    this.valid.next(true);
  }

  private getAllFirms() {
    this.httpService.getAllFirms().then(
      (result: Firm[]) => {
        this.firms = result;
        if (this.firms.length === 1)
          this.account.firms = [result[0].id];
      }
    );
  }

  getFirmNameById(id: number) {
    return this.firms.find(x => x.id === id).name;
  }

}
