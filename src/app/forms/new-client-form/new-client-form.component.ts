import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Client} from '../../models/client';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-new-client-form',
  templateUrl: './new-client-form.component.html',
})
export class NewClientFormComponent implements OnInit {
  clientForm: FormGroup;
  @Input() client: Client;
  @Output() valid: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor(builder: FormBuilder) {
    this.clientForm = builder.group({});
  }

  ngOnInit() {
    this.clientForm.addControl('name', new FormControl(this.client.name,
      [Validators.required, Validators.minLength(2)]));
    this.clientForm.addControl('surname', new FormControl(this.client.surname,
      [Validators.required, Validators.minLength(2)]));
    this.clientForm.addControl('birthday', new FormControl(this.client.birthday));
    this.clientForm.addControl('cardId', new FormControl(this.client.cardId,
      Validators.required));
    this.valid.next(false);
    this.clientForm.valueChanges.subscribe(form => {
      this.valid.next(this.clientForm.valid);
    });
  }

}
