import {Component, Input, OnInit} from '@angular/core';
import {Promotion} from '../../models/promotion';

@Component({
  selector: 'app-promotion-summary',
  templateUrl: './promotion-summary.component.html',
})
export class PromotionSummaryComponent implements OnInit {
  @Input() promotion: Promotion;

  constructor() {
  }

  ngOnInit() {
  }

}
