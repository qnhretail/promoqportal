import {Component, Input, OnInit} from '@angular/core';
import {Ticket, TicketStatus} from '../../../models/ticket';
import {PromoqHttpService} from '../../../services/promoq-http.service';
import {Client} from '../../../models/client';

@Component({
  selector: '[app-client-details-ticket-row]',
  templateUrl: './client-details-ticket-row.component.html',
})
export class ClientDetailsTicketRowComponent implements OnInit {
  @Input() ticket: Ticket;
  public client: Client = null;

  constructor(private httpService: PromoqHttpService) { }

  ngOnInit() {
    this.getClient();
  }

  getTicketValue(ticket: Ticket) {
    let value = 0;
    ticket.articles.forEach(article => {
      return value += article.priceInclDiscount;
    });
    return value;
  }

  getClient() {
    this.httpService.getClientByClientCardId(this.ticket.clientCardNr)
      .then((client: Client) => this.client = client);
  }

  getTicketStatusString() {
    return TicketStatus.getTicketStatusString(this.ticket.ticketStatus);
  }
}
