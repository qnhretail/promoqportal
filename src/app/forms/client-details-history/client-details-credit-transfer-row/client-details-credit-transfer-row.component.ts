import {Component, Input, OnInit} from '@angular/core';
import {PromoqHttpService} from '../../../services/promoq-http.service';
import {Promotion} from '../../../models/promotion';
import {CreditTransfer} from "../../../models/transfer";

@Component({
  selector: '[app-client-details-credit-transfer-row]',
  templateUrl: './client-details-credit-transfer-row.component.html',
})
export class ClientDetailsCreditTransferRowComponent implements OnInit {
  @Input() public creditTransfer: CreditTransfer;
  public promotion: Promotion;
  public assignedBy: string;

  constructor(private httpService: PromoqHttpService) { }

  ngOnInit() {
    this.getAccount(this.creditTransfer.assignedBy);
  }

  private getAccount(id: number) {
    this.httpService.getAccount(id).then(
      (account: {username: string}) => this.assignedBy = account.username
    );
  }

}
