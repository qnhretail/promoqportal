import {Component, Input, OnInit} from '@angular/core';
import {ClientAccount} from '../../models/account';
import {Firm} from '../../models/firm';
import {PromoqHttpService} from '../../services/promoq-http.service';
import {Ticket} from '../../models/ticket';
import {MatDialog} from '@angular/material';
import {TicketDetailsDialogComponent} from '../../dialogs/ticket-details-dialog/ticket-details-dialog.component';
import {CreditTransfer, PointTransfer} from "../../models/transfer";

@Component({
  selector: 'app-client-details-history',
  templateUrl: './client-details-history.component.html',
})
export class ClientDetailsHistoryComponent implements OnInit {
  tickets: Ticket[] = [];
  loading: boolean;
  @Input() clientAccount: ClientAccount;
  public firms: Firm[] = [];
  public pointTransfers: PointTransfer[] = [];
  public creditTransfers: CreditTransfer[] = [];

  constructor(private httpService: PromoqHttpService,
              private dialog: MatDialog) { }

  ngOnInit() {
    this.loading = true;
    this.getClientTicketHistory();
    this.getClientPointTransferHistory();
    this.getClientCreditTransferHistory();
  }

  getClientTicketHistory(): any {
    this.httpService.getTicketHistoryForClient(this.clientAccount.id).then(
      (tickets: Ticket[]) => this.tickets = tickets
    );
  }

  showTicketDetails(ticket: Ticket) {
    this.dialog.open(TicketDetailsDialogComponent,
      {data: {ticket: ticket}, width: '90%'});
  }

  private getClientPointTransferHistory() {
    this.httpService.getPointTransferHistoryForClient(this.clientAccount.id).then(
      (pointTransfers: PointTransfer[]) => this.pointTransfers = pointTransfers
    );
  }

  private getClientCreditTransferHistory() {
    this.httpService.getCreditTransferHistoryForClient(this.clientAccount.id).then(
      (creditTransfers: CreditTransfer[]) => this.creditTransfers = creditTransfers
    );
  }

}
