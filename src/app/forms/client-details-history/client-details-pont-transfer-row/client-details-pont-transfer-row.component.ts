import {Component, Input, OnInit} from '@angular/core';
import {PromoqHttpService} from '../../../services/promoq-http.service';
import {Promotion} from '../../../models/promotion';
import {PointTransfer} from "../../../models/transfer";

@Component({
  selector: '[app-client-details-point-transfer-row]',
  templateUrl: './client-details-pont-transfer-row.component.html',
})
export class ClientDetailsPointTransferRowComponent implements OnInit {
  @Input() public pointTransfer: PointTransfer;
  public promotion: Promotion;
  public assignedBy: string;

  constructor(private httpService: PromoqHttpService) { }

  ngOnInit() {
    this.getAccount(this.pointTransfer.assignedBy);
    this.getPromotion(this.pointTransfer.promotion);
  }

  private getPromotion(id: number) {
    this.httpService.getPromotion(id).then(
      (promotion: Promotion) => this.promotion = promotion
    );
  }

  private getAccount(id: number) {
    this.httpService.getAccount(id).then(
      (account: {username: string}) => this.assignedBy = account.username
    );
  }

}
