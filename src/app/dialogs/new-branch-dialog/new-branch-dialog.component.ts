import {Component, OnInit} from '@angular/core';
import {MatDialogRef} from '@angular/material';
import {Branch} from '../../models/branch';
import {GlobalService} from "../../services/global.service";

@Component({
  selector: 'app-new-branch-dialog',
  templateUrl: './new-branch-dialog.component.html',
})
export class NewBranchDialog implements OnInit {
  branch: Branch = new Branch(null, null);
  title = 'New branch';

  constructor(private dialogRef: MatDialogRef<NewBranchDialog>,
              private global: GlobalService) {
  }

  ngOnInit() {
  }

  onSaveClicked() {
    this.dialogRef.close(
        this.branch
    );
  }

  onCancelClicked() {
    this.global.warningDialogIfClose(new Branch(null, null), this.branch, this.dialogRef);
  }

}
