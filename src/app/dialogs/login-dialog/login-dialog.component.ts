///<reference path="../../../../node_modules/@angular/core/src/metadata/lifecycle_hooks.d.ts"/>
import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {PromoqHttpService} from '../../services/promoq-http.service';
import {GlobalService} from '../../services/global.service';
import {User} from '../../models/user';
import {environment} from "../../../environments/environment";

@Component({
  selector: 'app-login-dialog',
  templateUrl: './login-dialog.component.html',
})
export class LoginDialog implements OnInit {
  username = '';
  password = '';
  badLogin = false;
  title = 'Login';

  constructor(public dialogRef: MatDialogRef<LoginDialog>,
              private httpService: PromoqHttpService,
              private globalService: GlobalService,
              @Inject(MAT_DIALOG_DATA) public data: any) {
  }

  private _hide: boolean;

  get hide(): boolean {
    return this._hide;
  }

  set hide(value: boolean) {
    this._hide = value;
  }

  ngOnInit(): void {
    this._hide = true;
  }

  onLoginClicked(): void {
    if (this.username !== '' && this.password !== '') {
      const authToken: string = environment.authType + btoa(this.username + ':' + this.password.trim());
      this.httpService.login(authToken).then(
        response => {
          this.globalService.setUser(new User(response['username'], response['roles']));
          this.close();
        },
        reject => {
          this.badLogin = true;
        });
    }
  }

  onCancelClicked(): void {
    this.close();
  }

  close(): void {
    this.dialogRef.close();
    this.username = this.password = '';
    this.badLogin = false;
  }
}
