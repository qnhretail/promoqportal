import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';

@Component({
  selector: 'app-password-dialog',
  templateUrl: './password-dialog.component.html',
})
export class PasswordDialog implements OnInit {
  public password: string;
  public title = 'New password';

  constructor(private dialogRef: MatDialogRef<PasswordDialog>,
              @Inject(MAT_DIALOG_DATA) public data: {password: string}) {
  }

  ngOnInit() {
    this.password = this.data.password;
  }

  onOkClicked() {
    this.close(true);
  }

  onCancelClicked() {
    this.close(false);
  }

  close(result: boolean) {
    this.dialogRef.close(result);
  }

}
