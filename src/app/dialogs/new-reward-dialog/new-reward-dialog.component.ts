import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material';
import {Firm} from '../../models/firm';
import {User} from '../../models/user';
import {Reward} from '../../models/reward';
import {NewProductDialog} from '../new-product-dialog/new-product-dialog.component';
import {isUndefined} from 'util';
import {Product} from '../../models/product';
import {PromoqHttpService} from '../../services/promoq-http.service';
import {GlobalService} from '../../services/global.service';

@Component({
  selector: 'app-new-reward-dialog',
  templateUrl: './new-reward-dialog.component.html',
})
export class NewRewardDialog implements OnInit {
  private loggedInUser: User;
  private firm: Firm;
  public selectedProduct: number;
  public reward: Reward;
  public products: Product[];
  public title = 'new Reward';

  constructor(private httpService: PromoqHttpService,
              private global: GlobalService,
              private dialog: MatDialog,
              private dialogRef: MatDialogRef<NewRewardDialog>,
              @Inject(MAT_DIALOG_DATA) private data: { firm: Firm, loggedInUser: User }) {
  }

  ngOnInit() {
    this.loggedInUser = this.data.loggedInUser;
    this.firm = this.data.firm;
    this.reward = new Reward();
    this.products = [];
    this.reward.firmId = this.firm.id;
    this.reward.name = '';
    this.reward.id = null;
  }

  onAddProductClicked() {

  }

  onNewProductClicked() {
    this.dialog.open(NewProductDialog)
      .afterClosed().subscribe(
      (response: Product) => {

        if (!isUndefined(response)) {
          response.firmId = this.firm.id;
          this.addProduct(response);
        }
      }
    );
  }

  addProduct(product: Product) {
    this.httpService.addProduct(product).then(
      (response: Product) => this.products.unshift(response),
      error => this.onAddProductFailed(product)
    );
  }

  onAddProductFailed(product: Product) {
    this.global.warningDialogTryAgain('adding', 'product', product.name)
      .then(ok => this.addProduct(product));
  }

  getProductNameForId(id: number): string {
    return 'Not implemented';
  }

  onDeleteProductClicked(id: number) {

  }

  onSaveClicked() {
    this.dialogRef.close(this.reward);
  }

  onCancelClicked() {
    this.global.warningDialogIfClose(new Reward(), this.reward, this.dialogRef);
  }
}
