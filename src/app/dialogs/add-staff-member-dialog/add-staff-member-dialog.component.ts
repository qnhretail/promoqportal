import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {StaffMember} from '../../models/staff-member';
import {PromoqHttpService} from '../../services/promoq-http.service';
import {User} from '../../models/user';
import {GlobalService} from "../../services/global.service";

@Component({
  selector: 'app-add-staff-member-dialog',
  templateUrl: './add-staff-member-dialog.component.html',
})
export class AddStaffMemberDialog implements OnInit {
  staffMembers: StaffMember[] = [];
  selectedStaffMember: StaffMember = null;
  title = 'Add staff member';

  constructor(public dialogRef: MatDialogRef<AddStaffMemberDialog>,
              private httpService: PromoqHttpService,
              private global: GlobalService,
              @Inject(MAT_DIALOG_DATA) private data: { firmId: number, loggedInUser: User }) {
  }

  ngOnInit() {
    this.getAllStaffMembers();
  }

  private getAllStaffMembers() {
    this.httpService.getAllStaffMemberFromFirm(this.data.firmId).then(
      (response: StaffMember[]) => this.staffMembers = response,
      error => null
    );
  }

  onSaveClicked() {
    this.dialogRef.close(this.selectedStaffMember);
  }

  onCancelClicked() {
    this.global.warningDialogIfClose(null, this.selectedStaffMember, this.dialogRef);
  }
}
