import {Component, OnInit} from '@angular/core';
import {StaffMember} from '../../models/staff-member';
import {MatDialogRef} from '@angular/material';
import {GlobalService} from "../../services/global.service";

@Component({
  selector: 'app-new-staff-member-dialog',
  templateUrl: './new-staff-member-dialog.component.html',
})
export class NewStaffMemberDialog implements OnInit {
  staffMember: StaffMember = new StaffMember();
  public title = 'New staff member';

  constructor(private dialogRef: MatDialogRef<NewStaffMemberDialog>,
              private global: GlobalService) {
  }

  ngOnInit() {
  }

  onSaveClicked() {
    this.dialogRef.close(this.staffMember);
  }

  onCancelClicked() {
    this.global.warningDialogIfClose(new StaffMember(), this.staffMember, this.dialogRef);
  }

}
