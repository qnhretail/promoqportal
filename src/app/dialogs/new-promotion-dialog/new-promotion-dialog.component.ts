import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {Promotion} from '../../models/promotion';
import {Firm} from '../../models/firm';
import {GlobalService} from '../../services/global.service';
import {PromoType} from '../../models/promo-type';
import {PromoTypes} from '../../promo-types';

@Component({
  selector: 'app-new-promotion-dialog',
  templateUrl: './new-promotion-dialog.component.html',
})
export class NewPromotionDialog implements OnInit {
  promotion: Promotion;
  firm: Firm;
  title: string;
  TITLE_NEW_PROMOTION = 'New promotion';
  TITLE_EDIT_PROMOTION = 'Edit promotion';
  descriptionValid = true;
  configurationValid = false;
  branchesValid = false;
  private promoTypes: PromoType[];
  private valuePromoTypes: PromoType[];

  constructor(private dialogRef: MatDialogRef<NewPromotionDialog>,
              private global: GlobalService,
              @Inject(MAT_DIALOG_DATA) public data: { promotion: Promotion, firm: Firm }) {
  }

  ngOnInit() {
    this.firm = this.data.firm;
    if (typeof this.data.promotion === 'undefined' || !this.data.promotion) {
      this.title = this.TITLE_NEW_PROMOTION;
      this.data.promotion = new Promotion();
      this.data.promotion.type = 0;
      this.data.promotion.valuePromotionType = 0;
      this.data.promotion.firmPromotion = false;
      this.data.promotion.branches = [];
      this.data.promotion.firmId = this.firm.id;
    } else {
      this.title = this.TITLE_EDIT_PROMOTION;
      this.promoTypes = PromoTypes.promoTypes;
      this.valuePromoTypes = PromoTypes.valuePromoTypes;
    }
    this.promotion = this.global.clone(this.data.promotion);
  }

  onSaveClicked() {
    this.dialogRef.close(this.promotion);
  }

  onCancelClicked() {
    this.global.warningDialogIfClose(this.data.promotion, this.promotion, this.dialogRef);
  }

  validateDescription(event: boolean) {
    this.descriptionValid = event;
  }

  validateConfiguration(event: boolean) {
    this.configurationValid = this.descriptionValid && event;
  }

  validateBranches(event: boolean) {
    this.branchesValid = this.configurationValid && event;
  }

  checkRequiredFields(): boolean {
    this.branchesValid = true;
    return this.branchesValid;
  }

  public isNewPromotion(): boolean {
    return this.title === this.TITLE_NEW_PROMOTION;
  }
}
