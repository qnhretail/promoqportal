import {Component, Inject, OnInit} from '@angular/core';
import {User} from '../../models/user';
import {ClientAccount} from '../../models/account';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material';
import {Loading} from '../../hooks';
import {Client} from '../../models/client';
import {TranslateService} from '@ngx-translate/core';
import {PromoqHttpService} from '../../services/promoq-http.service';
import {GlobalService} from '../../services/global.service';
import {ChangePasswordDialogComponent} from '../change-password-dialog/change-password-dialog.component';

@Component({
  selector: 'app-client-details-dialog',
  templateUrl: './client-details-dialog.component.html',
})
export class ClientDetailsDialog implements Loading, OnInit {
  public clients: Client[];
  public loading: boolean;
  public loggedInUser: User;
  public title = this.translate.instant('Client details');
  public clientAccount: ClientAccount;
  public isClientAccountChanged = false;

  constructor(private dialogRef: MatDialogRef<ClientDetailsDialog>,
              public translate: TranslateService,
              private httpService: PromoqHttpService,
              public global: GlobalService,
              private dialog: MatDialog,
              @Inject(MAT_DIALOG_DATA) public data: { loggedInUser: User, clientAccount: ClientAccount }) {
  }

  ngOnInit() {
    this.loggedInUser = this.data.loggedInUser;
    this.clientAccount = this.global.clone(this.data.clientAccount);
  }

  onCancelClicked() {
    this.global.warningDialogIfClose(this.data.clientAccount, this.clientAccount, this.dialogRef);
  }

  validateAccount(event: boolean) {
    if (event === true) {
      if (JSON.stringify(this.clientAccount) !== JSON.stringify(this.data.clientAccount)) {
        this.isClientAccountChanged = true;
        return;
      }
    }
    this.isClientAccountChanged = false;
  }

  onSaveChangesClicked() {
    this.saveChanges();
  }

  private onSaveChangesFailed() {
    this.global.warningDialogTryAgain('saving', 'account', this.clientAccount.username)
      .then( ok => this.saveChanges());
  }

  public onChangePasswordClicked() {
    this.dialog.open(ChangePasswordDialogComponent);
  }

  saveChanges() {
    this.httpService.updateClientAccount(this.clientAccount).then(
      (account: ClientAccount) => {
        this.data.clientAccount = account;
        this.clientAccount = this.global.clone(account);
      },
      error => {
        this.onSaveChangesFailed();
      }
    );
  }
}
