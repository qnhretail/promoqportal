import {Component, Inject, OnInit} from '@angular/core';
import {User} from '../../models/user';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {Client} from '../../models/client';
import {ClientAccount} from '../../models/account';
import {GlobalService} from '../../services/global.service';

@Component({
  selector: 'app-new-client-account-dialog',
  templateUrl: './new-client-account-dialog.component.html',
})
export class NewClientAccountDialog implements OnInit {
  public clientDetailsValid = false;
  public loggedInUser: User;
  public title = 'New client';
  public account: ClientAccount;
  public client: Client;
  private formValid: boolean[] = [false, false, false]; //[newAccount, newClient, clientfirms]

  constructor(private dialogRef: MatDialogRef<NewClientAccountDialog>,
              private global: GlobalService,
              @Inject(MAT_DIALOG_DATA) private data: { loggedInUser: User }) {
  }

  ngOnInit() {
    this.loggedInUser = this.data.loggedInUser;
    this.account = new ClientAccount();
    this.client = new Client();
  }

  onCancelClicked() {
    this.global.warningDialogIfClose(
      {clientAccount: new ClientAccount(), client: new Client()},
      {clientAccount: this.account, client: this.client},
      this.dialogRef
    );
  }

  onSaveClicked() {
    this.client.email = this.account.email;
    this.account.name = this.client.name;
    this.dialogRef.close(
      {account: this.account, client: this.client});
  }

  checkRequiredFields() {
    this.clientDetailsValid = (this.formValid[0] && this.formValid[1]);
    if (this.clientDetailsValid) {
      return this.formValid[2];
    }
    return false;
  }

  validateFirms(event: boolean) {
    this.formValid[2] = event;
    this.checkRequiredFields();
  }

  validateClient(event: boolean) {
    this.formValid[1] = event;
    this.checkRequiredFields();
  }

  validateAccount(event: boolean) {
    this.formValid[0] = event;
    this.checkRequiredFields();
  }
}
