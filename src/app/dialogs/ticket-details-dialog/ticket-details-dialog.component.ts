import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {Ticket} from '../../models/ticket';
import {GlobalService} from '../../services/global.service';

@Component({
  selector: 'app-ticket-details-dialog',
  templateUrl: './ticket-details-dialog.component.html',
})
export class TicketDetailsDialogComponent implements OnInit {
  public title = 'Ticket details';
  public ticket: Ticket;

  constructor(private dialogRef: MatDialogRef<TicketDetailsDialogComponent>,
              private global: GlobalService,
              @Inject(MAT_DIALOG_DATA) public data: {ticket: Ticket}) { }

  ngOnInit() {
    this.ticket = this.data.ticket;
  }

  onCancelClicked() {
    this.global.warningDialogIfClose(this.data.ticket, this.ticket, this.dialogRef);
  }

}
