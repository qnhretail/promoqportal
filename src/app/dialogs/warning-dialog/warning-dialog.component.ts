import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';

@Component({
  selector: 'app-warning-dialog',
  templateUrl: './warning-dialog.component.html',
})
export class WarningDialog implements OnInit {
  public msg: string;
  public title = 'Warning';
  public yesNo: boolean;

  constructor(private dialogRef: MatDialogRef<WarningDialog>,
              @Inject(MAT_DIALOG_DATA) public data: { message: string, title: string, yesNo: boolean }) {
  }

  ngOnInit() {
    this.msg = this.data.message;
    this.yesNo = this.data.yesNo === null ? true : this.data.yesNo;
  }

  onYesClicked() {
    this.close(true);
  }

  onNoClicked() {
    this.close(false);
  }

  onCancelClicked() {
    this.close(false);
  }

  close(result: boolean) {
    this.dialogRef.close(result);
  }

}
