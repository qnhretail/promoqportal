import {Component, Inject, OnInit} from '@angular/core';
import {GlobalService} from '../../services/global.service';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {PromoqHttpService} from '../../services/promoq-http.service';
import {PasswordService} from "../../services/password.service";

@Component({
  selector: 'app-change-password-dialog',
  templateUrl: './change-password-dialog.component.html',
})
export class ChangePasswordDialogComponent implements OnInit {
  public title = 'Change password';
  public oldPassword = '';
  public newPassword =  '';
  public newPasswordCheck = '';
  public hide1 = true;
  public hide2 = true;
  public hide3 = true;

  constructor(public dialogRef: MatDialogRef<ChangePasswordDialogComponent>,
              public httpService: PromoqHttpService,
              public global: GlobalService,
              private passwordService: PasswordService,
              @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit() {
  }

  public onCancelClicked() {
    this.global.warningDialogIfClose(null, this.newPassword, this.dialogRef);
  }

  public onChangePasswordClicked() {
    if (this.passwordService.validatePassword(this.newPassword) && this.newPassword.trim() === this.newPasswordCheck.trim()) {
      this.httpService.setNewPassword(this.oldPassword.trim(), this.newPassword.trim()).then(
        (response) => {
          this.dialogRef.close();
        }
      );
    } else {
      this.global.warningDialogOneButton('The provided passwords are faulty ore the new password is shorter than 6 characters.');
    }
  }
}
