import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";
import {Product} from "../../models/product";
import {GlobalService} from "../../services/global.service";
import {Firm} from "../../models/firm";

@Component({
  selector: 'app-new-product-dialog',
  templateUrl: './new-product-dialog.component.html',
})
export class NewProductDialog implements OnInit {
  public product: Product;
  title: string;
  TITLE_NEW_PRODUCT = 'New product';
  TITLE_EDIT_PRODUCT = 'Edit product';

  constructor(private dialogRef: MatDialogRef<NewProductDialog>,
              private global: GlobalService,
              @Inject(MAT_DIALOG_DATA) public data: { product: Product, firm: Firm }) {
  }

  ngOnInit() {
    if (typeof this.data.product === 'undefined' || !this.data.product) {
      this.title = this.TITLE_NEW_PRODUCT;
      this.data.product = new Product();
    } else {
      this.title = this.TITLE_EDIT_PRODUCT;
      this.product = this.global.clone(this.data.product);
    }
    this.product = this.global.clone(this.data.product);
  }

  onSaveClicked() {
    this.dialogRef.close(this.product);
  }

  onCancelClicked() {
    this.global.warningDialogIfClose(this.data.product, this.product, this.dialogRef);
  }

}
