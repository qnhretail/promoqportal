import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {Amount} from '../../models/amount';
import {GlobalService} from '../../services/global.service';

@Component({
  selector: 'app-asign-points-to-client-dialog',
  templateUrl: './assign-points-to-client-dialog.component.html',
})
export class AssignPointsToClientDialog implements OnInit {
  public title = 'Assign points';
  public amount: Amount;
  private reason: string;

  constructor(private dialogRef: MatDialogRef<AssignPointsToClientDialog>,
              private global: GlobalService,
              @Inject(MAT_DIALOG_DATA) private data: { clientAccountId: number, promotionId: number}) {
  }

  ngOnInit() {
    if (this.data.promotionId === null) this.title = 'Assign credit';
    this.amount = new Amount(null, null);
  }

  onCancelClicked() {
    this.global.warningDialogIfClose(new Amount(null, null), this.amount, this.dialogRef);
  }

  onAssignPointsClicked() {
    this.dialogRef.close( this.amount);
  }
}
