import {Component, OnInit} from '@angular/core';
import {MatDialogRef} from '@angular/material';
import {GlobalService} from '../../services/global.service';

@Component({
  selector: 'app-dialog-firm',
  templateUrl: './new-firm-dialog.component.html',
})
export class NewFirmDialog implements OnInit {
  userRole: String = '';
  owners: any[];
  firmName: string = null;
  assignedTo: any = null;
  title: String = 'New firm';

  constructor(private dialogRef: MatDialogRef<NewFirmDialog>,
              private global: GlobalService) {
  }

  ngOnInit() {
  }

  onSaveClicked() {
    this.dialogRef.close(
      {
        name: this.firmName,
        assignedTo: this.assignedTo
      });
  }

  onCancelClicked() {
    this.global.warningDialogIfClose(
      {firm: null, at: null},
      {firm: this.firmName, at: this.assignedTo},
      this.dialogRef
    );
  }

}
