import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-mat-dialog-title',
  templateUrl: './mat-dialog-title.component.html'
})
export class MatDialogTitleComponent implements OnInit {
  @Input() title: string;
  @Output() onCancelClickedEmitter: EventEmitter<any> = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  public onCancelClicked(): void {
    this.onCancelClickedEmitter.emit();
  }

}
