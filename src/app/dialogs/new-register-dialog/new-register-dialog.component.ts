import {Component, OnInit} from '@angular/core';
import {MatDialogRef} from '@angular/material';
import {Register} from '../../models/Register';
import {GlobalService} from '../../services/global.service';

@Component({
  selector: 'app-new-register-dialog',
  templateUrl: './new-register-dialog.component.html',
})
export class NewRegisterDialog implements OnInit {
  register: Register;
  title= 'New Register';

  constructor(private dialogRef: MatDialogRef<NewRegisterDialog>,
              private global: GlobalService) {
  }

  ngOnInit() {
    this.register = new Register();
  }

  onSaveClicked() {
    this.dialogRef.close(this.register);
  }

  onCancelClicked() {
    this.global.warningDialogIfClose(new Register(), this.register, this.dialogRef);
  }
}
