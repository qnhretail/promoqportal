import {Component, Inject, OnInit} from '@angular/core';
import {User} from '../../models/user';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {Client} from '../../models/client';
import {GlobalService} from '../../services/global.service';

@Component({
  selector: 'app-new-client-dialog',
  templateUrl: './new-client-dialog.component.html',
})
export class NewClientDialog implements OnInit {
  public clientDetailsValid = false;
  public loggedInUser: User;
  public title = 'New client';
  public client: Client;
  public formValid = false;

  constructor(private dialogRef: MatDialogRef<NewClientDialog>,
              private global: GlobalService,
              @Inject(MAT_DIALOG_DATA) private data: { loggedInUser: User, client: Client }) {
  }

  ngOnInit() {
    this.loggedInUser = this.data.loggedInUser;
    if (this.data.client === null ) this.data.client = new Client();
    this.client = this.data.client;
  }

  onCancelClicked() {
    this.global.warningDialogIfClose(this.data.client, this.client, this.dialogRef);
  }

  onSaveClicked() {
    this.dialogRef.close(
      {client: this.client});
  }

  validateClient(event: boolean) {
    this.formValid = event;
  }
}
