import {Component, Inject, OnInit} from '@angular/core';
import {MatDialogRef} from '@angular/material/dialog';
import {MAT_DIALOG_DATA} from '@angular/material';
import {Branch} from '../../models/branch';
import {GlobalService} from '../../services/global.service';

@Component({
  selector: 'app-branch-details-dialog',
  templateUrl: './branch-details-dialog.component.html',
})
export class BranchDetailsDialog implements OnInit {
  originalBranch: Branch;
  public title = 'Branch details';
  public branch: Branch;

  constructor(public dialogRef: MatDialogRef<BranchDetailsDialog>,
              public global: GlobalService,
              @Inject(MAT_DIALOG_DATA) private data: { branch: Branch }) {
  }


  public ngOnInit() {
    this.branch = Object.assign({}, this.data.branch);
    this.originalBranch = this.data.branch;
  }

  public onSaveChangesClicked() {
    this.dialogRef.close(this.branch);
  }

  public onCancelClicked() {
    this.global.warningDialogIfClose(this.originalBranch, this.branch, this.dialogRef);
  }

}
