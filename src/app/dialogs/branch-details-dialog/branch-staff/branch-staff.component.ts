import {Component, Input} from '@angular/core';
import {StaffMember} from '../../../models/staff-member';
import {User} from '../../../models/user';
import {PromoqHttpService} from '../../../services/promoq-http.service';
import {MatDialog} from '@angular/material';
import {Branch} from '../../../models/branch';
import {isUndefined} from 'util';
import {AddStaffMemberDialog} from '../../../dialogs/add-staff-member-dialog/add-staff-member-dialog.component';
import {GlobalService} from '../../../services/global.service';
import {Loading} from '../../../hooks';

@Component({
  selector: 'app-branch-staff',
  templateUrl: './branch-staff.component.html',
})
export class BranchStaffComponent implements Loading {
  public loading: boolean;
  @Input() loggedInUser: User;
  @Input() branch: Branch;

  constructor(private httpService: PromoqHttpService,
              private global: GlobalService,
              private dialog: MatDialog) {
  }

  private _staff: StaffMember[] = [];

  get staff(): StaffMember[] {
    return this._staff;
  }

  set staff(value: StaffMember[]) {
    this._staff = value;
  }

  ngOnInit() {
    this.loading = true;
    this.getStaff();
  }

  public onAddStaffMemberClicked() {
    this.dialog.open(AddStaffMemberDialog, {
      data: {
        firmId: this.branch.firmId,
        loggedInUser: this.loggedInUser
      }
    }).afterClosed().subscribe(
      (response: StaffMember) => !isUndefined(response) ? this.addStaffMember(response) : null
    );
  }

  public onDeleteStaffMemberClicked(id: number) {
    this.global.warningDialogRemoval('staff member', id)
    .then( ok => {
          const staffMember = this.staff.find(x => x.id === id);
          this.staff = this.staff.filter(x => x !== staffMember);
          this.deleteStaffMember(staffMember);
        }
    );
  }

  private deleteStaffMember(staffMember: StaffMember) {
    this.httpService.deleteStaffMemberFromBranch(staffMember.id, this.branch.id).then(
      response => null, error => this.onDeleteStaffMemberFailed(staffMember));
  }

  private onDeleteStaffMemberFailed(staffMember: StaffMember) {
    this.staff.unshift(staffMember);
    this.global.warningDialogTryAgain('removing', 'staff member', staffMember.id)
      .then(ok => this.deleteStaffMember(staffMember));
  }

  private getStaff() {
    this.httpService.getStaffForBranch(this.branch.id).then(
      (response: StaffMember[]) => {
        this.staff = response;
        this.loading = false;
      }, error => this.loading = false
    );
  }

  private addStaffMember(staffMember: StaffMember) {
    this.httpService.addStaffMemberToBranch(staffMember.id, this.branch.id).then(
      (response: StaffMember) => this.staff.unshift(response),
      error => {
        if (error.status === 304) // 304: Not Modified
          this.global.warningDialogYesNo(
            'the Selected staff member is already included, would you like to select someone else?')
            .then( ok => this.onAddStaffMemberClicked());
        else this.onAddStaffMemberFailed(staffMember);
      }
    );
  }

  private onAddStaffMemberFailed(staffMember: StaffMember) {
    this.global.warningDialogTryAgain('adding', 'staff member', staffMember.username)
      .then( ok => this.addStaffMember(staffMember));
  }
}
