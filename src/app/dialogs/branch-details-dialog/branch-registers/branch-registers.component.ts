import {Component, Input} from '@angular/core';
import {MatDialog} from '@angular/material';
import {Branch} from '../../../models/branch';
import {PromoqHttpService} from '../../../services/promoq-http.service';
import {NewRegisterDialog} from '../../../dialogs/new-register-dialog/new-register-dialog.component';
import {isUndefined} from 'util';
import {GlobalService} from '../../../services/global.service';
import {Loading} from '../../../hooks';
import {Register} from "../../../models/Register";

@Component({
  selector: 'app-branch-registers',
  templateUrl: './branch-registers.component.html',
})
export class BranchRegistersComponent implements Loading {
  public loading: boolean;
  @Input() branch: Branch;

  constructor(private dialog: MatDialog,
              public global: GlobalService,
              private httpService: PromoqHttpService) {
  }

  private _registers: Register[] = [];

  get registers(): Register[] {
    return this._registers;
  }

  set registers(value: Register[]) {
    this._registers = value;
  }

  ngOnInit() {
    this.loading = true;
    this.getRegisters();
  }

  public onDeleteRegisterClicked(id: number) {
    if (this.global.isLoggedIn()) {
      this.global.warningDialogRemoval('register', id)
        .then( ok => {
            const register = this.registers.find(x => x.id === id);
            this.registers = this.registers.filter(x => x !== register);
            this.deleteRegister(register);
          }
      );
    }
  }

  public onAddRegisterClicked() {
    this.dialog.open(NewRegisterDialog).afterClosed().subscribe(
      (response: Register) => {
        if (!isUndefined(response)) {
          response.branchId = this.branch.id;
          this.addRegister(response);
        }
      }
    );
  }

  private getRegisters() {
    this.httpService.getAllRegistersForBranch(this.branch.id).then(
      (response: Register[]) => {
        this.registers = response;
        this.loading = false;
      }, error => this.loading = false
    );
  }

  private deleteRegister(register: Register) {
    this.httpService.deleteRegister(register.id).then(
      response => {
      },
      error => this.onDeleteRegisterFailed(register)
    );
  }

  private onDeleteRegisterFailed(register: Register) {
    this.registers.unshift(register);
    this.global.warningDialogTryAgain('removing', 'register', register.id)
      .then( ok => this.deleteRegister(register)
    );
  }

  private addRegister(register: Register) {
    this.httpService.addRegister(register).then(
      (response: Register) => this.registers.unshift(response),
      error => this.onAddRegisterFailed(register)
    );
  }

  private onAddRegisterFailed(register: Register) {
    this.global.warningDialogTryAgain('adding', 'register', register.id)
      .then( ok => this.addRegister(register)
    );
  }

  onCancelClicked() {

  }
}
