export class Product {
  private _id: number;
  private _name: string;
  private _productCode: string;
  private _firmId: number;
  public articleGroup: number;

  get firmId(): number {
    return this._firmId;
  }

  set firmId(value: number) {
    this._firmId = value;
  }

  get id(): number {
    return this._id;
  }

  set id(value: number) {
    this._id = value;
  }

  get productCode(): string {
    return this._productCode;
  }

  set productCode(value: string) {
    this._productCode = value;
  }

  get name(): string {
    return this._name;
  }

  set name(value: string) {
    this._name = value;
  }
}
