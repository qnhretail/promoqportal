export class StaffMember {
  id: number;
  username: string;
  name: string;
  surname: string;
  email: string;
  firmId: number;
}
