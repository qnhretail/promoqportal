import {TicketProduct} from './ticket-product';

export class TicketProductGroup {
  constructor(ticketProduct: TicketProduct) {
    this.plu = ticketProduct.plu;
    this.description = ticketProduct.description;
    this.ticketProducts = [];
  }
  public plu: string;
  public description: string;
  public ticketProducts: TicketProduct[];
  get price() {
    let price = 0;
    this.ticketProducts.forEach(x => price += x.price );
    return price;
  }
  get priceInclDiscount() {
    let price = 0;
    this.ticketProducts.forEach(x => price += x.priceInclDiscount );
    return price;
  }
}
