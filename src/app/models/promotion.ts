export class Promotion {

  public id: number;
  public name: string;
  public description: string;
  public firmId: number;
  public branches: number[];
  public type: number;
  public firmPromotion: boolean;
  public valueOfPoint: number;
  public valueGift: number;
  public validFrom: Date;
  public validUntil: Date;
  public validateUntil: Date;
  public giftArticleId: number;
  public valuePromotionType: number;
  public giftAmount: number;
}
