export class TicketProduct {
  public articleGroup: number;
  public plu: string;
  public description: string;
  public price: number;
  public priceInclDiscount: number;
}
