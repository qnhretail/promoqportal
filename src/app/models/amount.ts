export class Amount {
  public amount: number;
  public reason: string;

  constructor(amount: number, reason: string) {
    this.reason = reason;
    this.amount = amount;
  }
}
