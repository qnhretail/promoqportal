export class Account {
  public id: number;
  public username: string;
  public password: string;
}

export class ClientAccount extends Account {
  public name: string;
  public email: string;
  public phone: string;
  public firms: number[];
  public address: string;
  public zipCode: string;
  public town: string;
  public note: string;
}
