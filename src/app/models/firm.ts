import {Branch} from "./branch";

export class Firm {
  id: number;
  name: string;
  branches: Branch[];
  assignedTo;
}
