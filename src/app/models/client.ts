export class Client {
  public id: number;
  public name: string;
  public surname: string;
  public username: string;
  public email: string;
  public birthday: Date;
  public accountId: number;
  public cardId: string;
}
