export class SmtpServerConfiguration {
  public username: string;
  public password: string;
  public host: string;
  public port: string;
  public auth: boolean;
  public startTls: boolean;
}
