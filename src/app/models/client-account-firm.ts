export class ClientAccountFirm {
  clientId: number;
  firmId: number;
  firmName: string;
  receivePromotions: boolean;
  receiveNewsletter: boolean;
  receiveNewsletterOrig: boolean;
  receivePromotionsOrig: boolean;
  credit: number;
}
