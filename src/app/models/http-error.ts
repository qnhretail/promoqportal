
export class HttpError implements Error{
  public name: string;
  public message: string;
  public status: number;
  public ok: boolean;
  public url: string;
  public statusText: string;
  public error: string;
}
