/**
 * Created by Gert Pellin on 11/10/2017.
 */

export class User {
  public username: string;
  public roles: any[];

  public constructor(username: string, roles: any[]) {
    this.username = username;
    this.roles = roles;
  }
}
