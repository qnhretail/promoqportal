import {Product} from "./product";

export class Reward {
  public firmId: number;
  public id: number;
  public name: string;
  public products: Product[];
}
