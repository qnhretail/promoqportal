/**
 * Created by Gert Pellin on 11/10/2017.
 */

export class MenuItem {
  constructor(title: string, href: string) {
    this._title = title;
    this._href = href;
  }

  private _title: string;

  get title(): string {
    return this._title;
  }

  set title(value: string) {
    this._title = value;
  }

  private _href: string;

  get href(): string {
    return this._href;
  }

  set href(value: string) {
    this._href = value;
  }
}
