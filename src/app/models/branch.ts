export class Branch {
  id: number;
  name: string;
  address: string;
  email: string;
  phone: string;
  site: string;
  firmId: number;
  registerIds: Object[];

  constructor(name: string, firmId: number) {
    this.name = name;
    this.firmId = firmId;
  }
}
