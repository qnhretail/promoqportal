import {TicketProduct} from './ticket-product';
import {TicketPromotion} from './ticket-promotion';

export class Ticket {
  public id: number;
  public registerSerialNr: string;
  public clientCardNr: string;
  public staffMember: string;
  public ticketNr: string;
  public dateTime: Date;
  public articles: TicketProduct[];
  public promotions: TicketPromotion[];
  public ticketStatus: string;
}

export class TicketStatus {
  public static S10 = 'Registration pending';
  public static S15 = 'Registration in progress';
  public static S20 = 'Identification of client pending';
  public static S25 = 'Identification in progress';
  public static S30 = 'Handling pending';
  public static S35 = 'Handling in progress';
  public static S40 = 'Payment pending';
  public static S45 = 'Payment in progress';
  public static S50 = 'Settlement pending';
  public static S55 = 'Settlement in progress';
  public static S60 = 'Settled';
  public static S70 = 'Canceled';

  public static getTicketStatusString(status: string): string {
    switch (status) {
      case 'S10':
        return this.S10;
      case 'S15':
        return this.S15;
      case 'S20':
        return this.S20;
      case 'S25':
        return this.S25;
      case 'S30':
        return this.S30;
      case 'S35':
        return this.S35;
      case 'S40':
        return this.S40;
      case 'S45':
        return this.S45;
      case 'S50':
        return this.S50;
      case 'S55':
        return this.S55;
      case 'S60':
        return this.S60;
      default:
        return this.S70;
    }
  }
}
