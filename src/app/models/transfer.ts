export class Transfer {
  public id: number;
  public dateTime: Date;
  public quantity: number;
  public assignedBy: number;
  public clientAccount: number;
  public reason: string;
}

export class CreditTransfer extends Transfer {
  public firm: number;
}

export class PointTransfer extends Transfer {
  public promotion: number;
}
