import {MenuItem} from "./menuItem";

/**
 * Created by Gert Pellin on 11/10/2017.
 */

export class Menu {
  constructor(role: string) {
    this._role = role;
  }

  private _role: string;

  get role(): string {
    return this._role;
  }

  set role(value: string) {
    this._role = value;
  }

  private _menu: MenuItem[];

  get menu(): MenuItem[] {
    return this._menu;
  }

  set menu(value: MenuItem[]) {
    this._menu = value;
  }
}
