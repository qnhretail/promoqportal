import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {RouterModule, Routes} from '@angular/router';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';

import {AppComponent} from './app.component';
import {NavbarComponent} from './header/navbar/navbar.component';
import {HeaderComponent} from './header/header.component';
import {HomePageComponent} from './pages/home-page/home.component';
import {
  DateAdapter,
  MAT_DATE_FORMATS,
  MatAutocompleteModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatDatepickerModule,
  MatDialogModule,
  MatExpansionModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatRadioModule,
  MatRippleModule,
  MatSelectModule,
  MatSidenavModule,
  MatSliderModule,
  MatSlideToggleModule,
  MatSnackBarModule,
  MatSortModule,
  MatStepperModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule,
  MatTooltipModule,
} from '@angular/material';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {CdkTableModule} from '@angular/cdk/table';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {GlobalService} from './services/global.service';
import {PromoqHttpService} from './services/promoq-http.service';
import {CookieService} from 'ngx-cookie-service';
import {APP_BASE_HREF, CommonModule} from '@angular/common';
import {NewFirmDialog} from './dialogs/new-firm-dialog/new-firm-dialog.component';
import {WarningDialog} from './dialogs/warning-dialog/warning-dialog.component';
import {NewBranchDialog} from './dialogs/new-branch-dialog/new-branch-dialog.component';
import {LoginDialog} from './dialogs/login-dialog/login-dialog.component';
import {BranchDetailsDialog} from './dialogs/branch-details-dialog/branch-details-dialog.component';
import {BranchesFirmsPageComponent} from './pages/branches-firms-page/branches-firms.component';
import {BranchesComponent} from './pages/branches-firms-page/branches/branches.component';
import {NewRegisterDialog} from './dialogs/new-register-dialog/new-register-dialog.component';
import {StaffPageComponent} from './pages/staff-page/staff-page.component';
import {StaffListComponent} from './pages/staff-page/staff-list/staff-list.component';
import {BranchRegistersComponent} from './dialogs/branch-details-dialog/branch-registers/branch-registers.component';
import {BranchStaffComponent} from './dialogs/branch-details-dialog/branch-staff/branch-staff.component';
import {AddStaffMemberDialog} from './dialogs/add-staff-member-dialog/add-staff-member-dialog.component';
import 'hammerjs';
import {NewStaffMemberDialog} from './dialogs/new-staff-member-dialog/new-staff-member-dialog.component';
import {PromotionsPageComponent} from './pages/promotions-page/promotions-page.component';
import {PromotionsComponent} from './pages/promotions-page/promotions/promotions.component';
import {NewPromotionDialog} from './dialogs/new-promotion-dialog/new-promotion-dialog.component';
import {PromotionDescriptionFormComponent} from './forms/promotion-description-form/promotion-description-form.component';
import {PromotionConfigurationFormComponent} from './forms/promotion-configuration-form/promotion-configuration-form.component';
import {PromotionSummaryComponent} from './forms/promotion-summary/promotion-summary.component';
import {PromotionBranchesFormComponent} from './forms/promotion-branches-form/promotion-branches-form.component';
import {NewRewardDialog} from './dialogs/new-reward-dialog/new-reward-dialog.component';
import {NewProductDialog} from './dialogs/new-product-dialog/new-product-dialog.component';
import {ClientsPageComponent} from './pages/clients-page/clients-page.component';
import {NewClientAccountDialog} from './dialogs/new-client-account-dialog/new-client-account-dialog.component';
import {NewAccountFormComponent} from './forms/new-account-form/new-account-form.component';
import {NewClientFormComponent} from './forms/new-client-form/new-client-form.component';
import {ClientFirmsFormComponent} from './forms/client-firms-form/client-firms-form.component';
import {FirmPromotionsPanelComponent} from './pages/home-page/firm-promotions-panel/firm-promotions-panel.component';
import {PromotionComponent} from './pages/home-page/firm-promotions-panel/promotion/promotion.component';
import {BranchShortDescriptionComponent} from './pages/home-page/firm-promotions-panel/promotion/branch-short-description/branch-short-description.component';
import {Angular2DateAdapter, DDMMYYYYY_DATE_FORMATS} from './angular2-date-adapter';
import {ClientDetailsDialog} from './dialogs/client-details-dialog/client-details-dialog.component';
import {NewClientDialog} from './dialogs/new-client-dialog/new-client-dialog.component';
import {ClientClientcardsComponent} from './forms/client-details-clientcards-form/client-clientcards.component';
import {ClientFirmsComponent} from './forms/client-details-firms-form/client-firms.component';
import {MatDialogTitleComponent} from './dialogs/mat-dialog-title/mat-dialog-title.component';
import {ClickStopPropagationDirective} from './directives/click-stop-propagation.directive';
import {ConfigurationPageComponent} from './pages/configuration-page/configuration-page.component';
import {MailServerConfigurationPageComponent} from './pages/configuration-page/mail-server-configuration-page/mail-server-configuration-page.component';
import {BranchDetailsFormComponent} from './forms/branch-details-form/branch-details-form.component';
import {ClientDetailsPromotionsFormComponent} from './forms/client-details-promotions-form/client-details-promotions-form.component';
import {ClientDetailsPromotionsForFirmComponent} from './forms/client-details-promotions-form/client-details-promotions-for-firm/client-details-promotions-for-firm.component';
import {ClientDetailsPromotionComponent} from './forms/client-details-promotions-form/client-details-promotions-for-firm/client-details-promotion/client-details-promotion.component';
import {AssignPointsToClientDialog} from './dialogs/assign-points-to-client-dialog/assign-points-to-client-dialog.component';
import {ClientDetailsHistoryComponent} from './forms/client-details-history/client-details-history.component';
import {TooltipModule} from 'ngx-tooltip';
import {ClientDetailsTicketRowComponent} from './forms/client-details-history/client-details-ticket-row/client-details-ticket-row.component';
import {TicketDetailsDialogComponent} from './dialogs/ticket-details-dialog/ticket-details-dialog.component';
import {TicketDetailsProductListComponent} from './forms/ticket-details-product-list/ticket-details-product-list.component';
import {ClientDetailsTicketPromotionRowComponent} from './forms/client-details-ticket-promotion-row/client-details-ticket-promotion-row.component';
import {ProductsPageComponent} from './pages/products-page/products-page.component';
import {ProductsTableComponent} from './pages/products-page/products-table/products-table.component';
import {ClientDetailsPointTransferRowComponent} from './forms/client-details-history/client-details-pont-transfer-row/client-details-pont-transfer-row.component';
import {PasswordDialog} from './dialogs/password-dialog/password-dialog.component';
import {ChangePasswordDialogComponent} from './dialogs/change-password-dialog/change-password-dialog.component';
import {ClientDetailsCreditTransferRowComponent} from './forms/client-details-history/client-details-credit-transfer-row/client-details-credit-transfer-row.component';
import {PromotionDetailsConfigurationComponent} from './forms/promotion-details-configuration/promotion-details-configuration.component';
import {environment} from "../environments/environment";

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', ',json');
}

const routes: Routes = [
  {path: 'branches/:companyName', component: BranchesFirmsPageComponent},
  {path: 'staff/:companyName', component: StaffPageComponent},
  {path: 'promotions/:companyName', component: PromotionsPageComponent},
  {path: 'clients/:companyName', component: ClientsPageComponent},
  {path: 'configuration/:companyName', component: ConfigurationPageComponent},
  {path: 'products/:companyName', component: ProductsPageComponent},
  {path: ':companyName', component: HomePageComponent},
  {path: '**', component: HomePageComponent}
];

@NgModule({
  imports: [],
  exports: [
    CdkTableModule,
    MatAutocompleteModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatStepperModule,
    MatDatepickerModule,
    MatDialogModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
  ],
  declarations: [],
})
export class PlunkerMaterialModule {
}

@NgModule({
  declarations: [
    ClickStopPropagationDirective,
    MatDialogTitleComponent,
    AppComponent,
    NavbarComponent,
    HeaderComponent,
    HomePageComponent,
    FirmPromotionsPanelComponent,
    PromotionComponent,
    BranchShortDescriptionComponent,
    BranchesComponent,
    BranchesFirmsPageComponent,
    StaffPageComponent,
    ClientsPageComponent,
    ClientClientcardsComponent,
    ClientFirmsComponent,
    PromotionsPageComponent,
    ConfigurationPageComponent,
    MailServerConfigurationPageComponent,
    StaffListComponent,
    BranchRegistersComponent,
    BranchStaffComponent,
    LoginDialog,
    WarningDialog,
    PasswordDialog,
    NewFirmDialog,
    BranchDetailsDialog,
    NewRegisterDialog,
    AddStaffMemberDialog,
    NewStaffMemberDialog,
    NewBranchDialog,
    NewPromotionDialog,
    NewRewardDialog,
    NewProductDialog,
    NewClientAccountDialog,
    NewClientDialog,
    ClientDetailsDialog,
    AssignPointsToClientDialog,
    TicketDetailsDialogComponent,
    NewAccountFormComponent,
    NewClientFormComponent,
    ClientFirmsFormComponent,
    PromotionDescriptionFormComponent,
    PromotionConfigurationFormComponent,
    PromotionBranchesFormComponent,
    PromotionSummaryComponent,
    PromotionsComponent,
    BranchDetailsFormComponent,
    ClientDetailsPromotionsFormComponent,
    ClientDetailsPromotionsForFirmComponent,
    ClientDetailsPromotionComponent,
    ClientDetailsHistoryComponent,
    ClientDetailsTicketRowComponent,
    TicketDetailsProductListComponent,
    ClientDetailsTicketPromotionRowComponent,
    ProductsPageComponent,
    ProductsTableComponent,
    ClientDetailsPointTransferRowComponent,
    ClientDetailsCreditTransferRowComponent,
    ChangePasswordDialogComponent,
    PromotionDetailsConfigurationComponent
  ],
  exports: [
    ClickStopPropagationDirective
  ],
  imports: [
    BrowserModule,
    CommonModule,
    FormsModule,
    BrowserAnimationsModule,
    HttpClientModule,
    PlunkerMaterialModule,
    ReactiveFormsModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    RouterModule.forRoot(routes),
    TooltipModule
  ],
  entryComponents: [
  ],
  providers: [
    {provide: DateAdapter, useClass: Angular2DateAdapter},
    {provide: MAT_DATE_FORMATS, useValue: DDMMYYYYY_DATE_FORMATS},
    {provide: APP_BASE_HREF, useValue: environment.baseHref},
    GlobalService,
    CookieService,
    PromoqHttpService
  ],
  bootstrap: [AppComponent,
    ClientDetailsDialog,
    LoginDialog,
    NewFirmDialog,
    WarningDialog,
    PasswordDialog,
    BranchDetailsDialog,
    NewRegisterDialog,
    AddStaffMemberDialog,
    NewPromotionDialog,
    NewStaffMemberDialog,
    NewRewardDialog,
    NewProductDialog,
    NewClientAccountDialog,
    NewClientDialog,
    NewBranchDialog,
    TicketDetailsDialogComponent,
    AssignPointsToClientDialog,
    ChangePasswordDialogComponent
  ]
})


export class AppModule {
}
