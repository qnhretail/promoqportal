import {PromoType} from './models/promo-type';

export class PromoTypes {
  public static promoTypes = [new PromoType(0, 'Value Promotion')];
  public static valuePromoTypes = [
    new PromoType(0, 'Gift Article'),
    new PromoType(1, 'Direct Discount')
  ];
}
