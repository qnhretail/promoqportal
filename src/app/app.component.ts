import {AfterViewInit, Component} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {PromoqHttpService} from './services/promoq-http.service';
import {User} from './models/user';
import {GlobalService} from './services/global.service';
import {MatIconRegistry} from '@angular/material';
import {DomSanitizer} from '@angular/platform-browser';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements AfterViewInit {
  title = 'PromoQ';
  defaultLang = 'nl';

  constructor(private translate: TranslateService,
              private httpService: PromoqHttpService,
              private globalService: GlobalService,
              iconReg: MatIconRegistry,
              sanitizer: DomSanitizer) {

    // https://github.com/ngx-translate/core voor meer uitleg
    translate.setDefaultLang(this.defaultLang);
    const browserLang = translate.getBrowserLang();
    this.switchLanguage(browserLang);

    iconReg.addSvgIcon('logout',
      sanitizer.bypassSecurityTrustResourceUrl('assets/icons/logout-variant.svg'));
    iconReg.addSvgIcon('register',
      sanitizer.bypassSecurityTrustResourceUrl('assets/icons/television.svg'));
    iconReg.addSvgIcon('barcode',
      sanitizer.bypassSecurityTrustResourceUrl('assets/icons/barcode.svg'));
    iconReg.addSvgIcon('earth',
      sanitizer.bypassSecurityTrustResourceUrl('assets/icons/earth.svg'));

    this.globalService.init();
    this.httpService.loginWithCookie().then(
      response => {
        this.globalService.setUser(new User(response['username'], response['roles']));
      },
      error => {
        this.globalService.clearLogedinUser();
      }
    );
  }

  switchLanguage(language: string) {
    this.translate.use(language.match(/nl|fr|en|de/) ? language : this.defaultLang);
  }

  ngAfterViewInit() {
  }
}
