import {Subscription} from "rxjs/Subscription";
import {User} from "./models/user";

export interface Page {
  loggedInUserSubscription: Subscription;
  loggedInUser: User;
  loading: boolean;
  title: string;

  ngOnInit(): void;
}

export interface Loading {
  loading: boolean;

  ngOnInit(): void;
}
