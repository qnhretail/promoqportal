export const environment = {
  production: true,
  apiUrl: 'http://192.168.3.13:83/api/',
  authType: 'Basic ',

  baseHref: '/',

  bootstrap_xs: 767,

// ---- PromoQ ----
  logoSrc: 'assets/img/logo-promoq-White.png',
  bodyBg: 'none',

// ---- Wouters ----
  wouters: {
    logoSrc: 'http://www.bakkerij-wouters.be/wp-content/themes/victory-two/images/logo.png',
    bodyBg: 'url("http://www.bakkerij-wouters.be/wp-content/themes/victory-two/images/bg-tile.png")'
  }
};
