// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  apiUrl: 'http://localhost:8090/api/',
  authType: 'Basic ',

  baseHref: '/',

  bootstrap_xs: 767,

// ---- PromoQ ----
  logoSrc: 'assets/img/logo-promoq-White.png',
  bodyBg: 'none',

// ---- Wouters ----
  wouters: {
    logoSrc: 'http://www.bakkerij-wouters.be/wp-content/themes/victory-two/images/logo.png',
    bodyBg: 'url("http://www.bakkerij-wouters.be/wp-content/themes/victory-two/images/bg-tile.png")'
  }
};
