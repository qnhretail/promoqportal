|| [English](CHANGELOG.md) || [Nederlands](CHANGELOG_nl.md) || [français](CHANGELOG_fr.md) ||

# Change Log

## 0.1.0 (2018-02-21)

- Dialog with password for email-free customers
- Phone number for customer account
- Scale company image on customer portal on small screens
- styling for staff changes automatically 

### Bugfixes
- chrome popup will not show up with bad credentials

### Functionality
- logo and background can be easily changed globally or per company
- notes on clients can have multiple lines
- dutch: naam -> voornaam
- if enter is pressed on the login screen the form gets send
- manually assigned points can be seen in client history
- ticket-details and assigned points can be seen in client history
- points can be manually assigned per client and per promotion

## 0.0.1 Initial working version
