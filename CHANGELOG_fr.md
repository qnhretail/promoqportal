|| [English](CHANGELOG.md) || [Nederlands](CHANGELOG_nl.md) || [français](CHANGELOG_fr.md) ||

# Change Log

## 0.1.0 (2018-02-21)

- Dialogue avec mot de passe pour les clients sans email
- Numéro de téléphone du compte client
- Mettre à l'échelle l'image de l'entreprise sur le portail client sur les petits écrans
- Le style du personnel est automatiquement ajusté

### Bugfixes
- La fenêtre contextuelle chrome ne s'affichera pas avec des informations d'identification incorrectes

### Fonctionnalité
- Le logo et le fond peuvent être facilement changés globalement ou par compagnie
- les notes sur les clients peuvent avoir plusieurs lignes
- néerlandais: naam -> voornaam
- Si vous appuyez sur Entrée dans l'écran de connexion, le formulaire est envoyé
- les points attribués manuellement peuvent être vus dans l'historique du client
- les détails des tickets et les points attribués peuvent être vus dans l'historique du client
- Les points peuvent être attribués manuellement par client et par promotion

## 0.0.1 Version de travail initiale
