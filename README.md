# PromoQ

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.4.5.

## Configuration

### Translations

The site gets the language from the users browser. Current available languages are English, Dutch, German and French with English as in-code language. Only English and Dutch are fully translated. the translation files can be found in `src/assets/i18n`.

### Connection to backend

The connection to the backend application can be configured in the environment files. Best practice is to copy the prod environment file if you want to generate a new implementation. The environment files can be found in `src/environments/`.

### Branding

The site supports per firm branding, but this is not optimized (read automated) yet. Each company has to be added in the file `src/app/services/global.service.ts` inside the `setBranding()` function.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. 

Use the `-prod` flag for a production build. With the `--env=` flag you can select the environment you want to build for. The environment files can be found in `src/environments/` 

Build command example:

`ng-build --prod --env=staging`
